# PySN-lib ˈpaɪ-sən/ (Python for SN)

PySN-lib is an open-source Python script library designed to improve and or customize workflows involving Supernote devices.

- The 'Digest' module focuses on PDF annotation. While Supernote devices offer built-in PDF annotation capabilities, this PySN module aims to address some shortcomings and provide additional features to enhance the user's workflow. This tool is particularly useful for reviewing academic papers or any other PDFs that require extensive annotations.

## NOTICE & Credit: 

This code uses and alters [Github supernote-tool](https://github.com/jya-dev/supernote-tool/tree/master)
See changes at: [Supernotelib commits](https://gitlab.com/mmujynya/pysn-digest/-/commit/c8b9ca72c71293a666176405e1bc1fc21e90e0ba)


## WINDOWS

### Prerequisites

- Python 3.x (3.12 recommended)
- `pip` (Python package installer)


#### Checking if Python and pip are Already Installed

1. **Open Command Prompt (Windows) or Terminal (Mac/Linux)**
   
   Press `Win + R`, type `cmd`, and press `Enter`.

2. **Check Python Version**

   - Type `python --version` (Windows) and press `Enter`.
   - If you see a version number that starts with 3 (e.g., `Python 3.12.3`), then Python 3.x is already installed.

3. **Check pip Version**
   - Type `pip --version` (Windows) or `pip3 --version` (macOS & Linux) and press `Enter`.
   - If you see a version number (e.g., `pip 24`), then pip is already installed.
   - **Updating pip**: Even if pip is already installed, it’s a good idea to make sure it’s up-to-date. Run the following command:

        ```sh
        python -m pip install --upgrade pip
        ```
If Python 3.x or pip is not installed, follow the steps below to install them.


#### Installing Python 3.x and pip

1. **Download Python Installer**
   - Go to the official [Python website](https://www.python.org/downloads/).
   - Click the "Download Python 3.x.x" button (the version number might be different).

2. **Run the Installer**
   - Open the downloaded file to start the installation.
   - Check the box that says "Add Python 3.x to PATH".
   - Click "Install Now" and follow the on-screen instructions.

3. **Verify Installation**
   - Open Command Prompt and type `python --version` and `pip --version` to verify that both Python and pip are installed correctly.


### Setting Up the Virtual Environment

1. Open command prompt
2. Navigate to the project directory.
3. Run the setup script:

    ```sh
    python setup_venv.py
    ```

4. Follow the instructions provided by the script to activate the virtual environment. The script will output the command you need to run to activate the environment.

    - Normally:

        ```sh
        pysnvenv\Scripts\activate
        ```
    - If you have bash for Windows:   

        ```sh
        source pysnvenv/Scripts/activate
        ```        

### Usage

**IMPORTANT**: Prior to using digest.py for the first time, you should either define your **input folders**, the **output folder**, and the **export folder** or  ensure that the SN has the default settings for these directories. 

#### First time use (or changing settings in the future)

- ***Activate the virtual environment*** `pysnvenv\Scripts\activate` (see above if using bash for Windows)

- ***Set the input folders*** (one or several folders containing the annotated pdfs, separated by space) by typing (*)

    ```sh
        python digest.py --input Document/pysn-input/ SDcard/pysn-input
    ```

- ***Set the output folder*** (the unique folder containing the processed annotated files pdfs) by typing (*)

    ```sh
        python digest.py --output Document/pysn-output
    ```

- ***Set the export folder*** (the unique folder containing apdf of a merged original pdf + .mark) by typing (*)

    ```sh
        python digest.py --export EXPORT/pysn-export
    ```

(*) Replace 'Document/pysn-input/', 'SDcard/pysn-input/', 'Document/pysn-output' and 'EXPORT/pysn-export' by existing folders on your Supernote. Type `python digest.py --help` for CLI help.

Alternatively, you could also use the CLI help (first activate the virtual environment, then type  `python3 digest.py --help`), read the current values listed in brackets [] at the end of --input, --output and --export and create these folders, iff applicable, on your SN.

#### Routine use

- Using a shortcut. It should automatically activate the virtual environment and lauch the script

- Using a task scheduler (MS Windows task scheduler). When set properly, it should run within the virtual environment (*come back later for step by step instructions*)

- Using the CLI (Command Line Interface), ***after*** activating the virtual environment
    ```sh
        python digest.py
    ```



### Settings

The most used global variables can be updated in the CLI (Command Line Interface).

- To display the available settings through the CLI and their meaning, type

    ```sh
        python digest.py --help
    ```

There are many other global variable (in UPPERCASE) in the script, available for more customization.
All global variables are listed at the beginning of the script. See information and warning in the associated comments.



## MACOS & LINUX


### Prerequisites

- Python 3.x (3.12 recommended)
- `pip3` (Python package installer)

### Checking if Python and pip are Already Installed

1. **Open Terminal**
   - **Mac**: Press `Command + Space`, type `Terminal`, and press `Enter`.
   - **Linux**: Open your Terminal application from the application menu.

2. **Check Python Version**
   - Type `python3 --version` and press `Enter`.
   - If you see a version number that starts with 3 (e.g., `Python 3.12.3`), then Python 3.x is already installed.
   *Old macOS have Python 2.x have installed; that version is not compatible with PySN and you need to install Python 3.x*

3. **Check pip Version**
   - Type `pip3 --version`and press `Enter`.
   - If you see a version number (e.g., `pip 24`), then pip is already installed.

If Python 3.x or pip is not installed, follow the steps below to install them.

### Installing Python 3.x and pip 3.x

1. **Download Python Installer**
   - Go to the official [Python website](https://www.python.org/downloads/).
   - Click the "Download Python 3.x.x" button (the version number might be different).

2. **Run the Installer**
   - Open the downloaded file to start the installation.
   - Check the box that says "Add Python 3.x to PATH".
   - Click "Install Now" and follow the on-screen instructions.

3. **Verify Installation**
   - Open Command Prompt and type `python --version` and `pip --version` to verify that both Python and pip are installed correctly.

#### For Mac Users

- **Using Homebrew (Recommended)**
   - If Python 3.x is not installed, install Homebrew first (if not already installed):
     ```sh
     /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
     ```
   - After installing Homebrew, run:
     ```sh
     brew install python
     ```
   - This will install the latest version of Python 3 and pip.

- **Verify Installation**
   - Open Terminal and type `python3 --version` and `pip3 --version` to verify the installation.

#### For Linux Users

- **Using Package Manager**
   - Open Terminal and update your package list:
     ```sh
     sudo apt update
     ```
   - Install Python 3 and pip:
     ```sh
     sudo apt install python3 python3-pip
     ```

- **Verify Installation**
   - Open Terminal and type `python3 --version` and `pip3 --version` to verify the installation.

### Additional Tips

- **Updating pip**: Even if pip is already installed, it’s a good idea to make sure it’s up-to-date. Run the following command:
  ```sh
  python -m pip install --upgrade pip
  ```


### Setting Up the Virtual Environment

1. Open a terminal
2. Navigate to the project directory.
3. Run the setup script:

    ```sh
    python3 setup_venv.py
    ```    

4. Follow the instructions provided by the script to activate the virtual environment. The script will output the command you need to run to activate the environment.

    ```sh
    source pysnvenv/bin/activate
    ```
  

### Usage

**IMPORTANT**: Prior to using digest.py for the first time, you should either define your **input folders**, the **output folder**, and the **export folder** or  ensure that the SN has the default settings for these directories. 

#### First time use (or changing settings in the future)

- ***Activate the virtual environment*** (`source pysnvenv/bin/activate`)

- ***Set the input folders*** (one or several folders containing the annotated pdfs, separated by space) by typing (*)

    ```sh
        python3 digest.py --input Document/pysn-input/ SDcard/pysn-input
    ```

- ***Set the output folder*** (the unique folder containing the processed annotated files pdfs) by typing (*)

    ```sh
        python3 digest.py --output Document/pysn-output
    ```

- ***Set the export folder*** (the unique folder containing apdf of a merged original pdf + .mark) by typing (*)

    ```sh
        python3 digest.py --export EXPORT/pysn-export
    ```

(*) Replace 'Document/pysn-input/', 'SDcard/pysn-input/', 'Document/pysn-output' and 'EXPORT/pysn-export' by existing folders on your Supernote. Type `python3 digest.py --help` for CLI help.

Alternatively, you could also use the CLI help (first activate the virtual environment, then type  `python3 digest.py --help`), read the current values listed in brackets [] at the end of --input, --output and --export and create these folders, if applicable, on your SN.

#### Routine use

- Using a shortcut (it should automatically activate the virtual environment and launch the script). The installation script creates: 

    - for Windows: ***'PySN.lnk'***
    - for Linux:  ***'Run Python Script'***
    - for macOS: ***'PySN.command'***

- Using a task scheduler (MS Windows task scheduler, Mac Launchd or Linux cron). When set properly, it should run within the virtual environment

- Using the CLI (Command Line Interface), ***after*** activating the virtual environment

    ```sh
        python3 digest.py
    ```

### Settings

The most used global variables can be updated in the CLI (Command Line Interface).

- To display the available settings through the CLI and their meaning, type

    ```sh
        python3 digest.py --help
    ```

There are many other global variable (in UPPERCASE) in the script, available for more customization.
All global variables are listed at the beginning of the script. See information and warning in the associated comments.



## Test files and more information

YouTube video links and details steps on test files will be posted, come back later!

