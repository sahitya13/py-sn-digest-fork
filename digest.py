import sys
import os
import io
import shutil
import fitz
import json
import hashlib
from  datetime import datetime, timezone
from PIL import Image, ImageDraw
import numpy as np
from io import BytesIO
import supernotelib as sn
from supernotelib.converter import ImageConverter
from supernotelib.converter import VisibilityOverlay
from scipy.stats import mode

import scipy.ndimage as ndimage
import ctypes
from bs4 import BeautifulSoup
from urllib.parse import urljoin
import aiohttp
import asyncio
import aiofiles
import re
import platform
from ppadb.client import Client as AdbClient
import subprocess
import psutil
import mimetypes
import time
import requests
from datetime import datetime, timedelta, timezone
import argparse


# --------------------------------------------------------------------------------------
# Max Mujynya, May 27 2024
#---------------------------------------------------------------------------------------

# Some preps to hide the terminal poping up, for tasks 
# launched by the Windows task scheduler


# -------------------------------------------------------------
#  (*) Means "Don't change, unless you know what you are doing"  
#  (!) Means you SHOULD set this value for your machine/environment
# -------------------------------------------------------------

SUPER_HACK_MODE = True             # (!) Alters the source pdf and 'binds' it with the .mark file TODO: Probably not useful anymore, since this is the default
AUTO_EXPORT = True                 # (!) Creates a ready to export pdf (without marking)
USE_MS_VISION = True               # Optional handwritten text recognition us MS vision. Requires external account setup.
SHORTCUT_PATH = 'C:\\Users\\max\\OneDrive - mcicllc.com\\Desktop\\pysn-lib.lnk' # (!) Windows only: path to shortcut

# --------------------------------------------------------------------------------------------------
# IMAGE PROCESSING SETTINGS
#
HIGHLIGHTS_COLOR = {  
    'light gray' : (201, 201, 201),
    'dark gray' : (157,157,157),
    'white' : (254,254,254),
    'black': (0,0,0)}               # (*) Available color choices for 'Digest' done from highlights

DIGEST_COLOR = 'light gray'         # (*) Color used to 'select' items    
HIGHLIGHT_COLOR = 'dark gray'       # (*) Color used to 'highlight' items. Used on scans where highlighting not possible by SN  
ERASE_COLOR = 'white'               # (*) Color used to 'erase' items. Useful when editing a notebook that has already been exported to pdf 
NOTES_COLOR = 'black'               # (*) Color used for handwritten notes
TRANSPARENCY_LVL = 5
TRANSPARENCY_OTHER = 60
PROXIMITY_THRESHOLD = 35
PROXIMITY_NOTES = 35                # (*) Pixels proximity distance between handwritten notes. Used to merge adjacent words when selecting notes within digested are
RESOLUTION_FACTOR = 3               # Increasing this increases the quality of the crops, but also the size of the digested pdf

EXPAND_SHRINK_PXL = 6               # (*) This is the pixel amount used when expanding/contracting rect zones. Should be just a bit higher than space between letters but lower than space between words
DELTA_PXL_SEARCH = 8                # (*) Distance in pixels used when trying to define a rect snapping on pdf text
MAX_HORIZONTAL_PIXELS = 1404        # Default pixels size on A6x2. You may have to change that for other devices
MAX_VERTICAL_PIXELS = 1872          # Default pixels size on A6x2. You may have to change that for other devices

VERTICAL_GAP = 0                    # The gap we introduce when looking to the original text behind a rect
PROXIMITY_RECT_MERGE = 5            # The gap considered to merge two rects, in pixels

COLORCODE_BACKGROUND = 0x62         # (*)
COLORCODE_WHITE = 0x65              # (*)
SPECIAL_LENGTH = 0x4000             # (*)
SPECIAL_LENGTH_FOR_BLANK = 0x400    # (*)
SPECIAL_LENGTH_MARKER = 0xff        # (*)
# --------------------------------------------------------------------------------------------------
# EXPORT SIZE SETTINGS
#
PDF_LETTER_WIDTH = 612              # US Letter format
PDF_LETTER_HEIGHT = 792             # US Letter format
SUMMARY_REPORT_MARGIN = 50
NOTES_SHRINK_RATIO = 1.2            # Srinking ratio applied to handwritten notes
VERTICAL_REPORT_SPACING = 25
EMBED_SUMMARY = True                # (*) Untested for False, that should create 2 files: a 'digested' pdf and a separate 'digest summary'
SHOW_REC_NOTES = False              # TODO: Test if True. Should be True and at least on another layer when EMBED_SUMMARY is False
AUTHOR = "SN User"                  # (!) Replace with your name or initials. Used for comments.
DIGEST_LINE_HEIGHT = 30             # The guided lines for the digest summary section (set to <=0 if you don't want any)


#  --------------------------------------------------------------------------------------------------
# FILES AND FOLDERS LOCATION - INCLUDING CACHE CONTAINERS
#
PYSN_DIRECTORY = os.path.dirname(
    os.path.abspath(__file__))                          #  (*) Folder where this script is running
SN_VAULT = os.path.join(PYSN_DIRECTORY,'SN_vault')      #  (*) The vault is the working directory folder

PDF_CACHED = os.path.join(SN_VAULT,'cache')             #  Cached folder of original PDF files. Can improve execution time for large files on slow com
if not os.path.exists(PDF_CACHED):
    os.makedirs(PDF_CACHED)

SN_REGISTRY_FN = 'registry.json'                        #  (*) Name of the double dictionary, used to sore file hash and detect changes
USER_SETTINGS_FN = 'user_settings.json'                 #  (*) Name of last user's settings dictionary. Will be reused unless deleted
PIX_DICT_FN = os.path.join(SN_VAULT, 'pix_dict.json')   #  (*) Name of the dictionary used to save API calls for text recognition in previously submitted files

DEBUG_MODE = True                                       # Leaving to True will not delete temporary files.

#  --------------------------------------------------------------------------------------------------
# COMMON SETTINGS FOR USB, WIFI and FOLDERS. *** IMPORTANT: KEEP THE LINUX  folder notation with forward slash (whether or no you are in Windows) *****
#
SN_IO_FOLDER = '_sn_local'                              # (*) This is the short name of a vault subfolder. The script will populate this with with files.

SN_IO_WATCHED_FOLDERS = ['Document/pysn-input/',
                         'SDcard/pysn-input/']  #  (!) Relative path to the list of folders you want the script to monitor. Subfolders ignored. 
SN_IO_UPLOAD_FOLDER = 'Document/pysn-output'                  #  (!) Relative path to folder destination for the modified pdf and mark files. Subfolders ignored

NEVER_USE_CACHE_FILES = []                              #  (!) List of base filenames you don't want to be taken from the cache (like for different files with same name)
EXPORT_FOLDER = 'EXPORT/pysn-export'
#  --------------------------------------------------------------------------------------------------
# ADB SETTINGS (requires SN connected and prior download of ADB and folder 'platform-tools' installed in the same folder as this script
# https://developer.android.com/studio/releases/platform-tools
#
DEVICE_SN = "SNXXXXXXXXXXXX"                            # (!) Set Supernote serial number


ADB_FOLDER = os.path.join(
    PYSN_DIRECTORY, 'platform-tools')                   # (*) Full path to ADB folder. Change only if you have ADB already installed in another folder

if platform.system() == 'Windows':                      
    ADB_PATH = os.path.join(ADB_FOLDER, 'adb.exe')      # (*) Full path to ADB executable for Windows. Do not change
else:
    ADB_PATH = os.path.join(ADB_FOLDER, 'adb')          # (*) Full path to ADB executable for Mac OS & Linux. Do not change

                  
ADB_HOST = '127.0.0.1'
ADB_LOCAL_PORT = 5037                                   # (*) Default port
PDF_CACHED = os.path.join(SN_VAULT,'cache')             # (*) Full path to cached folder of pdf files
OVERWRITE_OUTPUT = True                                 # (!) When True, will attempt to overwrite existing output file. 

# ---------------------------------------------------------------------------------------------------
# LOCAL WIFI TRANSFER SETTINGS (requires SN switch "Browse & Access-On" to be active)
#
USE_DIRECT_WIFI_TRANSFER = True                         #  (*) True to enable this communication channel. *** AVOID ON PUBLIC NETWORKS ***
SN_IO_IP_ADDRESS = '192.168.4.232'                      #  (!) Local IP address displayed when "Browse & Access-On" is turned on
SN_IO_PORT = "8089"                                     #  (*) Port displayed when "Browse & Access-On" is turned on

# ---------------------------------------------------------------------------------------------------
# CLOUD SETTINGS. HERE ONEDRIVE. REMOTE FOLDERS ARE ACTUALLY LOCAL FOLDERS SYNCHRONIZED WITH THE CLOUD
#

REMOTE_BASE_PATH = 'C:\\Users\\max\\OneDrive - mcicllc.com\\Supernote\\'

AVS_WAIT = 3  # Time to wait between calls to Azure Vision to avoid hitting rate limits


# List of allowed global variable names,  from the CLI and storable in the user's settings
allowed_globals = {
    'AUTO_EXPORT', 'USE_MS_VISION', 'SHORTCUT_PATH', 'DIGEST_COLOR','ERASE_COLOR', 
    'PROXIMITY_THRESHOLD', 'PROXIMITY_NOTES', 'RESOLUTION_FACTOR', 'AUTHOR',
    'SN_IO_WATCHED_FOLDERS', 'SN_IO_UPLOAD_FOLDER', 'EXPORT_FOLDER','DEVICE_SN', 'ADB_FOLDER',
    'ADB_HOST', 'ADB_LOCAL_PORT', 'SN_IO_IP_ADDRESS', 'SN_IO_PORT', 'REMOTE_BASE_PATH', 'DEBUG_MODE'}
# Dictionary of global variables exposed to the CLI: option long-name, short-name, descrition and type
settings_desc_dict = {
    'debug': {
        'short': 'debug',
        'description': 'DEBUG_MODE      Enable (prefix -no- to disable) debug. When enabled, more info print on screen and temp folder is not deleted',
        'type': bool}, 
    'input': {
        'short': 'input',
        'description': 'One or several SN folder paths, separated by a space, indicating the location of the files to be processed. SD card paths must start with "SDcard". Subfolders are ignored.',
        'type': str},
    'output': {
        'short': 'output',
        'description': 'SN destination folder for the modified pdf and mark files. SD card path must start with "SDcard".',
        'type': str
    },
    'export': {
        'short': 'xp',
        'description': 'Relative path of SN destination folder where merged pdf and annotation will be uploaded',
        'type': str},
    'autoxp': { 
        'short': 'ae',
        'description': 'AUTO_EXPORT Enable (prefix -no- to disable) automatic export of a merged pdf (pdf+annotation merged)',
        'type': bool},
    'msvision': {
        'short': 'mv',
        'description': 'USE_MS_VISION       Enable (prefix -no- to disable) text recognition using MS vision. This functionality requires external account setup with Microsoft.',
        'type': bool},
    'wifi': {
        'short': 'web',
        'description': 'USE_DIRECT_WIFI_TRANSFER        Enable (prefix -no- to disable) transfer through wifi (webserver on SN)',
        'type': bool},          
    'shortcut': {
        'short': 'sc',
        'description': 'Windows only: path to shortcut', 'type': str},
    'selcolor': {
        'short': 'dc',
        'description': 'Color used to select pdf items',
        'type': str},
    'delcolor': {
        'short': 'ec',
        'description': 'Color used to erase pdf items',
        'type': str},
    'selpix': {
        'short': 'sp',
        'description': 'Selections within that pixel distance will be included in the digest',
        'type':int},
    'notepix': {
        'short': 'np',
        'description': 'Handwriting notes within that pixel distance will be included in the digest',
        'type':int},
    'res': {
        'short': 'r',
        'description': 'Increasing this increases the quality of the crops, but also the size of the digested pdf',
        'type':int},
    'auth': {
        'short': 'auth',
        'description': 'Author name showing in pdf comments', 
        'type': str},

    'serial': {
        'short': 'sn',
        'description': 'Serial number of the SN, read from the USB port. For future programming to avoid sync with unregistered SNs',
        'type': str},
    'adbf': {
        'short': 'adb_path',
        'description': 'Path to ADB binary. Set this if ADB is already on your machine',
        'type': str},
    'adbh': {
        'short': 'adb_host',
        'description': 'ADB local IP address',
        'type': str},    
    'adbp': {
        'short': 'adb_port',
        'description': 'ADB communication port',
        'type': str},  
    'webip': {
        'short': 'web_host',
        'description': 'SN local web IP address displayed when "Browse & Access-On" is turned on',
        'type': str},                              
    'webport': {
        'short': 'web_port',
        'description': 'SN local port number displayed when "Browse & Access-On" is turned on',
        'type': str},                              
    'syncf': {
        'short': 'mirror',
        'description': 'Local path to the mirrored root of the synch cloud folder',
        'type': str},                              
                            


}
# Dictionary of global variables exposed to the CLI: option long-name and global variable
param_to_global_var = {
    'autoxp': 'AUTO_EXPORT',
    'msvision': 'USE_MS_VISION',
    'shortcut': 'SHORTCUT_PATH',
    'selcolor': 'DIGEST_COLOR',
    'delcolor': 'ERASE_COLOR',
    'selpix': 'PROXIMITY_THRESHOLD',
    'notepix': 'PROXIMITY_NOTES',
    'res' : 'RESOLUTION_FACTOR',
    'auth': 'AUTHOR',
    'input': 'SN_IO_WATCHED_FOLDERS',
    'output': 'SN_IO_UPLOAD_FOLDER',
    'export':'EXPORT_FOLDER',
    'serial': 'DEVICE_SN',
    'adbf': 'ADB_FOLDER',
    'adbh': 'ADB_HOST',
    'adbp': 'ADB_LOCAL_PORT',
    'wifi': 'USE_DIRECT_WIFI_TRANSFER',
    'webip': 'SN_IO_IP_ADDRESS',
    'webport': 'SN_IO_PORT',
    'syncf': 'REMOTE_BASE_PATH',
    'debug': 'DEBUG_MODE'

}

# Visibility parameters for the png extraction (from Supernotelib library)
bg_visibility = VisibilityOverlay.DEFAULT
bg_invisibility = VisibilityOverlay.INVISIBLE
vo = sn.converter.build_visibility_overlay(background=bg_visibility)
vi = sn.converter.build_visibility_overlay(background=bg_invisibility)

def save_json(afilename, ajson):
    """ Saving a json with identation """ 
    with open(afilename, 'w') as file:
        file.write(json.dumps(ajson, indent=4)) 


def read_json(afilename):
    """ Reading a json from a filename """ 
    try:
        with open(afilename , 'r') as file:
            return json.loads(file.read().strip())
    except:
        return {}
 

def bytes_to_hex_string(byte_data):
    # Convert each byte to a two-character hexadecimal string
    hex_string = ' '.join(f'{byte:02x}' for byte in byte_data)
    return hex_string


def load_image_with_frombytes(png_file_path):
    # Open the image using Pillow
    with Image.open(png_file_path) as img:
        # Convert image to RGBA (if it's not already) to ensure consistent handling
        img = img.convert('RGBA')
        
        # Use io.BytesIO to handle the image data in memory
        img_byte_arr = io.BytesIO()
        img.save(img_byte_arr, format='PNG')  # Save image to the byte array in PNG format
        img_byte_arr.seek(0)  # Move to the start of the byte array

        # Read the image data back from the byte stream
        byte_data = img_byte_arr.read()

        # Decode the PNG data back into an image (for educational/demonstration purposes)
        # Note: Normally, you would not re-encode and then decode like this.
        new_img = Image.open(io.BytesIO(byte_data))
        new_img = new_img.convert('RGBA')  # Ensure format consistency

        return new_img


def erase_pixels(img, rect=None, color_to_erase=HIGHLIGHTS_COLOR[DIGEST_COLOR]):
    """ If a rect is given, erase the rectangle, otherwise erase pixel colors (by default digestion selection) """
    # Load the image
    img_array = np.array(img)

    # Determine the 'blank' pixel depending on image mode (RGBA or not)
    if img.mode == 'RGBA':
        color_to_erase = np.array(list(color_to_erase) + [255])  # Assuming full opacity for matching
        blank_pixel = [255, 255, 255, 0]  # Fully transparent for RGBA
    else:
        blank_pixel = 255  # White for grayscale or RGB

    # If a rect is given, erase the rectangle, otherwise erase pixel colors (by default digestion selection)
    if rect:
        # Define the rectangle boundaries
        x1, y1, x2, y2 = rect
        # Erase pixels in the rectangle by setting them to the 'blank' pixel
        img_array[y1:y2, x1:x2] = blank_pixel


    else:
        # Find all pixels matching the color to erase
        matches = np.all(img_array[:, :, :3] == color_to_erase[:3], axis=-1)

        # Apply the blank pixel to all matching pixels
        if img.mode == 'RGBA':
            img_array[matches] = blank_pixel
        else:
            # For non-RGBA images, ensure the blank pixel doesn't try to assign multiple values
            img_array[matches] = blank_pixel if img.mode != 'RGB' else [blank_pixel] * 3

    # Create a new image from the modified array
    new_img = Image.fromarray(img_array, img.mode)

    return new_img


def rle_encode_img(image):
    """ Ephemeral bitmap images are encoded in the .note and .mark files
        using a 'simple' RLE algorithm.
        Encodes image data in a run-length algorithm compatible with the
        supernote decode. This assumes that palette is none and blank is none
        TODO: comment all steps
    """
    encoded = False
    try:
        segments_list = []
        result = bytearray()
        # Convert the image to grayscale mode 'L'
        gray_image = image.convert('L')
        # Convert the grayscale image to a NumPy array
        img_array = np.array(gray_image)
        # Flatten the array to 1D
        pixels = img_array.flatten()
        # Detect where the value changes
        changes = np.diff(pixels) != 0
        change_indices = np.where(changes)[0] + 1
        # Include the start and end of the array
        segment_starts = np.concatenate(([0], change_indices))
        segment_ends = np.concatenate((change_indices, [len(pixels)]))

        large_threshold = int(str(SPECIAL_LENGTH))
        zip_data = zip(segment_starts, segment_ends)

        for start, end in zip_data:
            length_s = end - start
            color_s = pixels[start]
            colo = pixels[start]
            segments_list.append((start, color_s, length_s))
            if color_s == 255:
                color_s = COLORCODE_BACKGROUND      # 62
            elif color_s == 0:
                color_s = 0x66           
            elif color_s == 254:
                color_s = COLORCODE_WHITE           # 65
            elif color_s == 201:
                color_s = 0xCA
            elif color_s == 157:
                color_s = 0x9E       # 63
            else:
                color_s = 0xb7   # 
            
            if length_s > 127:
                q_128, r_128  = divmod(length_s, 128)
                if r_128 == 0:
                    result.extend([color_s] + list(bytes.fromhex(format(128+q_128-1, '02x'))) )
                else:
                    quotient, remainder = divmod(length_s, large_threshold)
                    result.extend([color_s, SPECIAL_LENGTH_MARKER]*quotient)
                    first_part = int((remainder-1)/128) + 127
                    second_part = remainder-1-((first_part % 127)*128)
                    b_1 = bytes.fromhex(format(first_part, '02x'))
                    b_2 = bytes.fromhex(format(second_part, '02x'))
                    if first_part == 127:
                        result.extend([color_s] + list(b_2))
                    else:
                        result.extend([color_s] + list(b_1) + [color_s] + list(b_2))
            else:
                try:
                    result.extend([color_s]+list(bytes.fromhex(format(length_s - 1, '02x'))))
                except Exception as e:
                    print(f'*** Error: {e}')
                    exit(1)
        encoded = True
    except Exception as e:
        print()
        print(f'*** Error in rle_encode_img: {e}')
        return None, encoded
    return result, encoded


def read_endian_int_at_position(data, position, num_bytes=4, endian='little'):
    """ Returns the endian integer equivalent of 'num_bytes' read
        from 'data' at 'position' """
    completed = False
    integer_value = 0

    try:
        # Ensures valid endian type
        if endian not in ['little', 'big']:
            raise ValueError("Endian must be 'little' or 'big'")
        
        # Ensure the position and number of bytes to read are within the data range
        if position < 0 or position + num_bytes > len(data):
            raise ValueError("Invalid position or number of bytes to read")
        
        # Read the specified number of bytes from the given position
        byte_sequence = data[position:position + num_bytes]

        # Convert the byte sequence from little-endian to an integer
        integer_value = int.from_bytes(byte_sequence, byteorder=endian)
        completed = True
    except Exception as e:
        if DEBUG_MODE:
            print()
            print(f'*** Error @ read_endian_int_at_position: {e}')
            print(f'>>> position: {position} - num_bytes: {num_bytes} - endian: {endian}')
    
    return completed, integer_value


def int_to_little_endian_bytes(value, num_bytes=4, byteorder='little'):
    # Ensures valid endian type
    if byteorder not in ['little', 'big']:
        raise ValueError("Endian must be 'little' or 'big'")
    # Ensure the value fits within the specified number of bytes
    if value < 0 or value >= (1 << (num_bytes * 8)):
        raise ValueError(f"Value {value} out of range for {num_bytes} bytes")
    
    # Convert the integer to a byte sequence in the specified endian format
    return value.to_bytes(num_bytes, byteorder=byteorder)


def replace_hex_bytes_in_file_block(binary_content, replacement_bytes, start_pos):
    """ Replaces block of bytes at a given position in the file.
        It is assumed that the 4 bytes at position hold the size of the block
        Proceed only if the current size of the block is larger than the replacement_hex size
        TODO: Change the address and move replacement bytes at the end of the file for larger blocks"""
    replaced = False
    # Read Current block size at position
    completed, current_block_size = read_endian_int_at_position(binary_content, start_pos)
    if completed:
        new_block_size = len(replacement_bytes)
        if new_block_size <= current_block_size:
            # Prepare the new block with the size header
            header_new_block = int_to_little_endian_bytes(new_block_size)
            new_block = header_new_block + replacement_bytes
            
            # Overwrite the block in the binary content
            new_binary_content = (binary_content[:start_pos] + 
                                new_block + 
                                binary_content[start_pos + 4 + new_block_size:])
            replaced = True
        else:
            new_binary_content = binary_content
    return replaced, new_binary_content


def mute_stroke_color(binary_data, position_list, colors=[HIGHLIGHTS_COLOR[DIGEST_COLOR][0], 202, 254]):
    """ Replace color of strokes in trailcontainer to 'FF' 
        This is not an ephemeral action and we do not really erase the strokes, we just make them invisible """
    completed_function = True
    muted_color =  int('FF', 16)
    try:
        # Convert the bytes object to a bytearray
        data = bytearray(binary_data)
        # Positionning of the byte for color in a given trail
        color_byte_pos = 5
        # Parse the list of 'Totalpath' positions
        for a_position in position_list:
            # Read the size of a trailcontainer (4 bytes)
            completed, tc_size = read_endian_int_at_position(data,a_position,num_bytes=4)

            if completed:
                # Read the number of trails in the container
                completed, tc_nb = read_endian_int_at_position(data,a_position+4,num_bytes=4)

                if completed:
                    tc_offset = 8

                    # Parse the number of trails in the container
                    for tc_index in range(tc_nb):
                        # read size of trail
                        completed, t_size = read_endian_int_at_position(data,a_position+tc_offset,num_bytes=4)

                        if completed:
                            # read cor_byte_value
                            byte_position = a_position+tc_offset+4+color_byte_pos-1

                            color_byte_value = data[byte_position]
                            if color_byte_value in colors:
                                data[byte_position] = muted_color

                            tc_offset += t_size + 4
                        else:
                            completed_function = False
                            break
                else:
                    completed_function = False
                    break
            else:
                completed_function = False
                break
    except Exception as e:
        print()
        print(f'*** Error in mute_stroke_color: {e}')
        return False, binary_data

    return completed_function, bytes(data)
            

def mute_page(binary_data, page_address):
    """ 'Deletes' marking of a page in the binary file.
        To meet our goal of minimalistic intervention in editing binaries, we don't actually delete
        the information, but we edit the size header of the page binaries to indicate that the file
         is empty (zero size). Since the pdf file and the mark files are 2 dinstinct files, when deleting
        a pdf page, the corresponding pen strokes in the mark file need to be muted or the SN will 'spill'
         these in the next pdf page. Muting the page addresses the issue. """
    try:
        size_zero =  b'\x00\x00\x00\x00'
        # Convert the bytes object to a bytearray
        data = bytearray(binary_data)
        # Set block size to zero
        data[page_address:page_address+4] = size_zero
    except Exception as e:
        print()
        print(f'*** Error in mute_page: {e}') 
    return bytes(data)


def xref_layer_by_name(doc, layer_name):
    """ Returns the 'layer_name' layer xref """
    ocgs_dict = doc.get_ocgs()
    for ocg, ocg_dict in ocgs_dict.items():
        if ocg_dict['name'] == layer_name:
            return ocg
    return None


def compare_lists(list1, list2):
    """ returns intersections and items specifics to each list"""
    # Convert lists of lists to sets of tuples to perform set operations
    set1 = set(tuple(item) for item in list1)
    set2 = set(tuple(item) for item in list2)
    
    # Calculate intersection
    common_items = [list(item) for item in set1 & set2]
    
    # Calculate items unique to list1
    specific_to_list1 = [list(item) for item in set1 - set2]
    
    # Calculate items unique to list2
    specific_to_list2 = [list(item) for item in set2 - set1]

    # Calculate union of the 2 lists
    union_list = list(set(tuple(x) for x in list1) | set(tuple(x) for x in list2))
    
    return common_items, specific_to_list1, specific_to_list2, union_list


def save_to_extended_metadata(pdfdoc, xref_meta, key, value):
    """ Save private information  the extended metadata """
    try:
        pdfdoc.xref_set_key(xref_meta, key, fitz.get_pdf_str(json.dumps(value)))
    except Exception as e:
        print()
        print(f'*** Error: {e}')
        print(f'Could not save {key} to extended metadata')


def extended_metadata(doc):
    """ Returns the reference to the 'info' data and the extended metadata, including private information"""
    metadata = {}  # make my own metadata dict
    what, value = doc.xref_get_key(-1, "Info")  # /Info key in the trailer
    if what != "xref":
        pass  # PDF has no metadata
    else:
        xref = int(value.replace("0 R", ""))  # extract the metadata xref
        for key in doc.xref_get_keys(xref):
            metadata[key] = doc.xref_get_key(xref, key)[1]
    return xref, metadata


def concatenate_text_and_get_rect(data):
    """ Concatenates text and return rect of container of a list of text, rect
        Used to aggregate CVision output """
    concatenated_text = ''
    min_x1, min_y1, max_x2, max_y2 = float('inf'), float('inf'), float('-inf'), float('-inf')
    
    for text, rect in data:
        concatenated_text += text + ' '
        x1, y1, x2, y2 = rect
        min_x1 = min(min_x1, x1)
        min_y1 = min(min_y1, y1)
        max_x2 = max(max_x2, x2)
        max_y2 = max(max_y2, y2)
    
    concatenated_text = concatenated_text.strip()
    overall_rect = [min_x1, min_y1, max_x2, max_y2]
    
    return concatenated_text, overall_rect


def pdf_datetime_to_timestamp(pdf_datetime):
    # Extract the main datetime components and the timezone offset
    date_str = pdf_datetime[2:16]  # Strip off the initial 'D:'
    sign = pdf_datetime[16]
    offset_hours = int(pdf_datetime[17:19])
    offset_minutes = int(pdf_datetime[20:22])
    
    # Create a datetime object from the string
    dt = datetime.strptime(date_str, '%Y%m%d%H%M%S')
    
    # Calculate the total offset in minutes
    total_offset = (offset_hours * 60 + offset_minutes) * (1 if sign == '+' else -1)
    
    # Apply the timezone offset
    tz = timezone(timedelta(minutes=total_offset))
    dt = dt.replace(tzinfo=tz)
    
    # Convert datetime to UTC
    dt_utc = dt.astimezone(timezone.utc)
    
    # Convert to Unix timestamp in milliseconds
    timestamp = int(dt_utc.timestamp() * 1000)
    
    return timestamp


def sorted_annots_toc(doc, type=None):
    """ Creates a sorted list of annotation by date """
    list_annots = []
    
    if not doc.has_annots():
        return
    else:
        doc_toc = doc.get_toc()
        existing_annots_toc = [(x[1],x[2]) for x in doc_toc ]
        for page in doc.pages():
            for annot in page.annots():
                if type is not None:
                    if annot.type != type:
                        continue
                annot_text = annot.get_text()
                annot_info = annot.info
                annot_date = ''
                if annot_info is not None:
                    if 'modDate' in annot_info.keys():
                        annot_date = annot_info['modDate']    #
                        if annot_date != '':
                            annot_date_ts = pdf_datetime_to_timestamp(annot_date)
                            annot_page = page.number+1
                            if annot_text == '':
                                annot_text = annot_info['content']
                                if annot_text == '':
                                    annot_text = annot_info['subject']
                                date_str = annot_date[2:16]
                                dt = datetime.strptime(date_str, '%Y%m%d%H%M%S')
                                standard_datetime = dt.strftime('%Y-%m-%d %H:%M:%S')
                                annot_text += '\n'+standard_datetime
                            a_bkmark = (annot_text,annot_page,annot_date_ts)
                            if  (annot_text,annot_page) not in existing_annots_toc:
                                list_annots.append(a_bkmark)
        
        list_annots.sort(key=lambda x: -x[2])
        toc_annote_date = [(2,x[0],x[1],x[2]) for x in list_annots ]
        if len(toc_annote_date) > 0:
            
            
            had_last_annots = False
            for a_toc in doc_toc:
                if a_toc[1] == 'Last annotations':
                    had_last_annots = True
                    break
            if not had_last_annots:
                toc_annote_date.insert(0,(1,'Last annotations',toc_annote_date[0][2],{"color":(1,0,0)}))
            doc_toc.extend(toc_annote_date)
            doc.set_toc(doc_toc, collapse=1)
    return 


def delete_annotation_at_rect(page, target_rect, target_color, tolerance=0.2):
    """
    Delete annotations at a specified rectangle and of a specified color.
    
    Args:
    target_rect (fitz.Rect): The rectangle area in which annotations should be checked.
    target_color (tuple): The RGB color of the annotations to delete (values 0-1).
    tolerance (float): The tolerance for color comparison.
    """
    annotations = page.annots()
    if annotations:
        for annot in annotations:
            if annot.rect.intersects(target_rect):  # Check if the rects intersect
                color = annot.colors.get("fill")  # Get the annotation's color
                if color and all(abs(color[i] - target_color[i]) <= tolerance for i in range(3)):
                    page.delete_annot(annot)


def intersection_area(rect1, rect2, threshold=0.95):
    # Unpack the coordinates of the rectangles

    x0,y0,x1,y1 = rect1
    a0,b0,a1,b1 = rect2

    rect1_area = (x1-x0)*(y1-y0)
    rect2_area = (a1-a0)*(b1-b0)
    rect_area = min(rect1_area, rect2_area)

    # Find the coordinates of the intersection rectangle
    x_left = max(x0, a0)
    y_bottom = max(y0, b0)
    x_right = min(x1, a1)
    y_top = min(y1, b1)

    # Check if there is no overlap
    if x_left > x_right or y_bottom > y_top:
        return False

    # Calculate the width and height of the intersection rectangle
    width = x_right - x_left
    height = y_top - y_bottom
    coverage = width * height / rect_area
    if coverage > threshold:
        return True
    return False


def rect_exists(page_settings,rect,threshold=0.80, rec_position=2, flag_position=5):
    """ Lookup in the page_settings if an entry is
        intersecting with the rect above the threshold. This function is useful to avoid 
        reprocessing links to a file that has them already built-in """
    try:
        result = False
        intersecting_rect_position = -1

        if 's_marker' in page_settings.keys():
            selected_rects = page_settings['s_marker']
            index = 0
            for a_rect_set in selected_rects:
                selected_rect = a_rect_set[rec_position]
                selected_rect_has_link = a_rect_set[flag_position] == 1
                if intersection_area(selected_rect, rect, threshold=threshold):
                    result = selected_rect_has_link
                    intersecting_rect_position = index
                    break
                index += 1
    except:
        print()
        print(f'*** Error in rect_exists for rect: {rect}')
    return result, intersecting_rect_position


def submit_png_to_cvision(file_path):
    """ Submits a png image to Microsoft Azure Computer Vision
        EndPoint. Details about the endpoint, as well as the API key,
        are stored in the environment variables.
        
        The endpoint contains reference to the resource that
        has been provisioned on Microsoft Azure
        """    
    cvision_response = False
    # Open the image file
    try:
        image = Image.open(file_path)
        # Convert the image to a NumPy array
        image_array = np.array(image)
        # Check if there is any non-zero pixel in the image
        try:
            if not np.any(np.all(image_array != [255, 255, 255], axis=-1)):
                return [], cvision_response
        except Exception as e:
            pass
    except Exception as e:
        print()
        print(f'*** Error in submit_png_to_cvision: {e}')
        return [], cvision_response

    # Loads environment variables
    global USE_MS_VISION
    DCHECKS_AVS_END_POINT = os.environ.get('DCHECKS_AVS_END_POINT')
    DCHECKS_AVS_KEY = os.environ.get('DCHECKS_AVS_KEY')

    url = f'{DCHECKS_AVS_END_POINT}/computervision/imageanalysis:analyze?api-version=2023-10-01&features=read&model-version=latest&language=en'  

    # Get the image content-type
    content_type, _ = mimetypes.guess_type(file_path)
    if content_type is None:
        content_type = 'application/octet-stream'

    # Format the header
    headers = {
        'Content-Type': content_type,
        'Ocp-Apim-Subscription-Key': DCHECKS_AVS_KEY}

    
    try:
        # Force wait to avoid API rate limit
        time.sleep(AVS_WAIT)

        # Open the image file in binary mode
        files = open(file_path, 'rb')

        # Make a POST request
        print()
        print(f'\n>>>> Contacting MS Computer Vision (sleeping {AVS_WAIT}s): {file_path[-50:]}', end='')
        response = requests.post(url=url, headers=headers, data=files)

        # Check the response. If valid, return the list of blocks
        if response.ok:
            print('\u2713',end='')
            print()
            aresp = json.loads(response.content.decode('utf-8'))
            cvision_response = True
            return aresp["readResult"]["blocks"], cvision_response
        else:
            print()
            return [], cvision_response
    except requests.exceptions.RequestException  as e:
        print()
        print(f"*** An error occurred: {e}... Disabling temporarily external recognition...")
        USE_MS_VISION = False
        return [], cvision_response


def linux_to_windows_paths(path_input):
    """ Makes a linux path or a list of paths compatible with Windows """
    if isinstance(path_input, list):
        return [p.replace('/', '\\') for p in path_input]
    else:
        return path_input.replace('/', '\\')


def stop_adb_if(original_state=False, path=ADB_PATH):
    """ Stops adb if original_state is False (i.e if adb was not running) OR if no parameter is provided.
        Parameters:
        - original_state: boolean.
        - path: path to the adb executable"""
    if not original_state:
        command = [path, 'kill-server']
        try:
            subprocess.run(command)
        except Exception as e:
            print(f"An error occurred: {e}")
            print(f'*** Failed to stop ADB by calling "{command}"')
            return
    return


def is_adb_running():
    """Check if ADB server is running by looking for its process."""
    for proc in psutil.process_iter(['name']):
        if proc.info['name'][:3] == 'adb':
            return True
    return False

def attached_sn(device_name=DEVICE_SN, path=ADB_PATH, host=ADB_HOST, port=ADB_LOCAL_PORT):
    """ Try to locate Supernote serial number on a USB port.
        Returns a SN device object and a boolean indicating if
         ADB was running before the connection 
         
         Parameters:
         - device_name    :   the serial number of the supernote, if available
         - path           :   path to the adb executable
         
         Returns:
         - A tuple consisting of: an adb device object and the state of ADB prior the call
         """
    
    exclude = ['List', 'of', 'devices', 'attached', 'device']  # List of keywords to exclude from output
    
    # Check if ADB server is already running
    adb_was_running = is_adb_running()
    if adb_was_running:
        print('>>> ADB is already running')
    else:
        print('>>> ADB is not running, trying to launch ADB server ...')
        try:
            subprocess.run([path, 'start-server'], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            print("*** Could not start ADB server:", e)
            return None, adb_was_running
    
    # Identify connected devices and connect to a sn, preferably device_name
    try:
        list_devices_cmd = [path, 'devices']
        result = subprocess.run(list_devices_cmd, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        command_result = result.stdout.split()
        device_serials = [x for x in command_result if x not in exclude and x[:2] == 'SN']
        if len(device_serials) > 0:
            if device_name in device_serials:
                device_sn_to_use = device_name
            else:
                device_sn_to_use = device_serials[0]
                print(f'*** Could not find device {device_name} but will try to connect to {device_sn_to_use} instead')
        else:
            print("*** No Supernote device found")
            return None, adb_was_running
    except subprocess.CalledProcessError as e:
        print("*** Failed to list devices:", e)
        return None, adb_was_running
    
    # Try to create an adb client device object
    try:
        client = AdbClient(host=host, port=port)
        device = client.device(device_sn_to_use)
    except Exception as e:
        print("*** No Supernote device found on USB:", e)
        return None, adb_was_running
    
    return device, adb_was_running

def get_file_system_in_userspace(device):
    """ Retrieve FUSE header information for internal
        storage and SD Card, when connecting through USB
        """
    internal_prefix = external_prefix = ''
    try:
        # We send a 'disk free human readable' request
        disk_free_request = device.shell("df -h")
        # FUSE entries contain '/dev/fuse' . We only keep those lines
        lines = disk_free_request.strip().split('\n')
        fuse_entries = [line.split()[-1] for line in lines if '/dev/fuse' in line]
        internal_prefix = [item for item in fuse_entries if 'emulated' in  item][0] + '/0/'
        external_prefix = [item for item in fuse_entries if 'emulated' not in  item][0] + '/'

    except Exception as e:
        print()
        print(f'*** Error in FUSE: {e}')
    return internal_prefix, external_prefix


def copy_files_from_device(device, source_folder, destination_folder,
                           cached_folder=PDF_CACHED, ignore_cache_list=NEVER_USE_CACHE_FILES):
    """ Copies files from a folder on adb client device to a destination folder.
        For pdf files, looks first in a cached folder before proceeding.
        Populates the cached folder for new pdf files
        We are only checking the filename to see if the cache should be used and this is
        not very reliable, thus the ignore_cache_list."""
    try:
        print()
        # Get the list of files in the source folder on the device


        source_folder_ = source_folder.replace(' ','\\ ')
        filestr = device.shell(f"ls {source_folder_}")
        filestr = filestr.replace('\n', '  ').replace('\\ ','%20')
        filestr_list = re.split(r'\s{2,}', filestr)
        files_list = [x for x in filestr_list if x != '']
        files_list.sort()

        # Iterate over each file and pull it to the destination folder
        for file_name in files_list:
            base_name = file_name.replace('%20',' ')
            source_path = os.path.join(source_folder, base_name)
            dest_path = os.path.join(destination_folder, base_name)
            ignore_cache_list_l = [x.lower() for x in ignore_cache_list]
            cache_exception = base_name.lower() in ignore_cache_list_l
            if source_path[-3:].lower() == 'pdf':
                cached_path = os.path.join(cached_folder, base_name)
                if os.path.exists(cached_path) and not cache_exception:
                    shutil.copy(cached_path, dest_path)
                else:
                    device.pull(source_path, dest_path)
                    print(f'  - downloaded:{base_name}')
                    if not cache_exception:
                        shutil.copy(dest_path, cached_path)
            else:
                device.pull(source_path, dest_path)
                print(f'  > downloaded:{base_name}')
    except Exception as e:
        print(f"An error occurred: {e}")
        print(f'*** Failed to copy "{source_folder}"')          


def delete_files_by_pattern(device, pattern, base_folder, folder=SN_IO_UPLOAD_FOLDER ):
    """ Deletes files of a certain pattern on attached usb device """
    try:
        pattern = pattern.replace(' ', '%20')
        # Construct and execute command to delete files matching the pattern
        delete_command = f"find {base_folder}{folder} -name '{pattern}' -exec rm {{}} +"
        result = device.shell(delete_command)
        
        # Print the result of the delete command
        print(result)

        # Optional: Confirm deletion or list remaining files
        remaining_files = device.shell(f"ls {base_folder}{folder}")
        rfl = remaining_files.split()
        rfl_ = [x.replace('%20',' ') for x in rfl]
        print("Remaining files in the directory:")
        print(rfl_)

    except Exception as e:
        print(f"An error occurred: {e}")
        print(f'*** Failed to delete file(s) like {pattern} on "{base_folder}{folder}"')   


def timestamp_to_pdf_datetime(timestamp):
    # Convert millisecond timestamp to seconds
    dt = datetime.fromtimestamp(timestamp / 1000)
    
    # Localize datetime to current timezone
    # This requires third-party libraries like pytz or dateutil if you need to handle time zones other than the local system's.
    local_dt = dt.astimezone()
    
    # Format the timezone offset
    # Offset returned is in seconds. Convert it to 'HH'mm'' format.
    offset_sec = local_dt.utcoffset().total_seconds()
    sign = '+' if offset_sec >= 0 else '-'
    offset_h = int(abs(offset_sec) // 3600)
    offset_m = int((abs(offset_sec) % 3600) // 60)
    
    # Format according to PDF specification
    pdf_datetime = local_dt.strftime(f"D:%Y%m%d%H%M%S{sign}{offset_h:02d}\'{offset_m:02d}\'")
    
    return pdf_datetime


def time_now_to_pdf_datetime():
    # Convert millisecond timestamp to seconds
    dt = datetime.now()
    
    # Localize datetime to current timezone
    # This requires third-party libraries like pytz or dateutil if you need to handle time zones other than the local system's.
    local_dt = dt.astimezone()
    
    # Format the timezone offset
    # Offset returned is in seconds. Convert it to 'HH'mm'' format.
    offset_sec = local_dt.utcoffset().total_seconds()
    sign = '+' if offset_sec >= 0 else '-'
    offset_h = int(abs(offset_sec) // 3600)
    offset_m = int((abs(offset_sec) % 3600) // 60)
    
    # Format according to PDF specification
    pdf_datetime = local_dt.strftime(f"D:%Y%m%d%H%M%S{sign}{offset_h:02d}\'{offset_m:02d}\'")
    
    return pdf_datetime


def hide_console_window():
    """ Windows platform only: Hide the terminal poping up, for tasks launched by 
        Windows task scheduler"""
    # Get the handle of the console window
    console_window = ctypes.windll.kernel32.GetConsoleWindow() if platform.system() == 'Windows' else None
    if console_window:
        # Hide the window
        ctypes.windll.user32.ShowWindow(console_window, 0)


def get_text_from_image(image_fn, inv_pr_matrix, pix_dict_fn=PIX_DICT_FN):
    """ If the hash signature of the file isn't found in the pix_dict 
        dictionary, submits the image to computer vision at Microsoft for
        text detection """
    pix_text = ''
    hw_ocr_list = []  
    if USE_MS_VISION:
        try:
            # Get the file hash signature
            image_fn_sha = file_sha(image_fn)
            # Retrieve its entry in the pix_dict, if available
            pix_dict = read_json(pix_dict_fn)
            if image_fn_sha in pix_dict.keys():
                saved_rec = pix_dict[image_fn_sha]
                pix_text = saved_rec['text']
                hw_ocr_list = saved_rec['map']
            else:
                apng_ocr, cvision_response = submit_png_to_cvision(image_fn)
                if len(apng_ocr)>0 or cvision_response:
                    if len(apng_ocr)>0:
                        apng_ocr_j = apng_ocr[0]
                        for element in apng_ocr_j['lines']:
                            a_text = element['text']
                            pix_text += a_text + ' '
                            a_bound = element['boundingPolygon']
                            x0, y0, x1, y1 = (a_bound[0]['x'], a_bound[0]['y'], a_bound[2]['x'], a_bound[2]['y'])
                            a_rect_s = fitz.Rect((x0, y0, x1, y1 ))*inv_pr_matrix
                            a_rect = [a_rect_s[i] for i in range(4)]
                            hw_ocr_list.append((a_text, a_rect))
                    pix_text = pix_text.strip()
                    pix_dict[image_fn_sha] = {"text":pix_text, "map":hw_ocr_list}
                    save_json(pix_dict_fn, pix_dict)
        except Exception as e:
            if DEBUG_MODE:
                print()
                print(f"An error occurred: {e}")
                print('*** Failed to get text from MS vision')
            return '', []
    return pix_text, hw_ocr_list


async def fetch(session, url):
    try:
        async with session.get(url) as response:
            return await response.text()
    except aiohttp.client_exceptions.ClientConnectorError as e:
        print(f"*** Failed to connect: {e} ***")
        print('>>> In the Supernote settings, you may want to check that the "Browse & Access-On" is turned on.')
        print()
        return None


async def download_file(session, file_url, destination):
    async with session.get(file_url) as response:
        if response.status == 200:
            print(f'>>> Wifi copy from: ...{file_url[20:60]}...{file_url[-35:]} to ...{destination[:20]}...{destination[-35:]}')

            data = await response.read()
            async with aiofiles.open(destination, 'wb') as f:
                await f.write(data)


async def upload_file(file_path, upload_url, filename):

    boundary = '----WebKitFormBoundary7MA4YWxkTrZu0gW'
    async with aiohttp.ClientSession() as session:
        # Read the file data
        async with aiofiles.open(file_path, 'rb') as f:
            file_data = await f.read()

        # Create multipart form body as bytes
        body = (
            f'--{boundary}\r\n'
            f'Content-Disposition: form-data; name="file"; filename="{filename}"\r\n'
            f'Content-Type: {mimetypes.guess_type(file_path)[0]}\r\n\r\n'
        ).encode() + file_data + (
            f'\r\n--{boundary}\r\n'
            f'Content-Disposition: form-data; name="fileinfo"\r\n'
            f'Content-Type: application/json\r\n\r\n'
            f'{{"name": "{filename}"}}\r\n'
            f'--{boundary}--\r\n'
        ).encode()

        headers = {
            'Content-Type': f'multipart/form-data; boundary={boundary}'
        }

        async with session.post(upload_url, data=body, headers=headers) as response:
            if response.status != 200:
                print(f"*** Error: Failed to upload {file_path}, status: {response.status}")
                # response_text = await response.text()
                # print(f"Response: {response_text}")
                

async def parse_and_download_files(url, base_folder):
    async with aiohttp.ClientSession() as session:
        html = await fetch(session, url)
        soup = BeautifulSoup(html, 'html.parser')

        # Find the script containing the data
        script_text = ''
        found = False
        for script in soup.find_all("script"):
            if 'const json =' in script.string or 'const json=' in script.string:  # Check if the script contains the JSON variable
                script_text = script.string
                found = True
                break
        
        if not found:
            print("No script with JSON data found.")
            return

        # Extract JSON data from the script
        match = re.search(r"('.*?')", script_text, re.DOTALL)
        if match:
            json_text = match.group(1)
            data = json.loads(json_text[1:-1])
        else:
            print("Failed to extract JSON data.")
            return

        # Asynchronously download all files
        tasks = []
        files_to_download = []
        for file in data['fileList']:
            if not file['isDirectory']:  # Ensure it's a file
                file_url = urljoin(url, file['uri'])
                file_nm = file['name']
                destination = os.path.join(base_folder,file_nm )
                if file_nm[-3:].lower() == 'pdf':
                    cached_destination = os.path.join(PDF_CACHED, file_nm)
                    if os.path.exists(cached_destination):
                        shutil.copy(cached_destination, destination)
                    else:
                        files_to_download.append((destination,cached_destination))
                        task = asyncio.create_task(download_file(session, file_url, destination))
                        tasks.append(task)
                else:
                    task = asyncio.create_task(download_file(session, file_url, destination))
                    tasks.append(task)

        await asyncio.gather(*tasks)
        for destination,cached_destination in files_to_download:
            if os.path.exists(destination):
                shutil.copy(destination, cached_destination)


async def async_upload(file_to_upload, upload_url, filename):
    await upload_file(file_to_upload, upload_url,filename)


def copy_rects(source_img, target_img, rects):
    """ Copies rect areas from source to target"""
    # Process each rectangle
    for x0, y0, x1, y1 in rects:
        # Define the box to copy from source image
        box =  (x0, y0, x1, y1)
        # Extract the region
        region = source_img.crop(box)
        # Paste the region into the target image
        target_img.paste(region, box)
    

def find_fonts_in_rect(rect, page):
    """
    Finds unique font names and sizes in the specified rectangle on a given page.

    Parameters:
    - rect: fitz.Rect, the rectangle within which to search for text.
    - page: Page object from a PyMuPDF document.

    Returns:
    - List of tuples, where each tuple contains (font name, font size).
    """
    # Ensure rect is a fitz.Rect object
    rect = fitz.Rect(rect)
    # Initialize a set to store unique (font, size) tuples
    unique_fonts = set()

    # Extract text blocks from the page
    text_blocks = page.get_text("dict")["blocks"]
    
    # Iterate through text blocks to find fonts in the specified rectangle
    for block in text_blocks:
        if "lines" in block:  # Ensure the block contains lines of text
            for line in block["lines"]:
                for span in line.get("spans", []):  # Iterate through each span in a line
                    span_rect = fitz.Rect(span["bbox"])  # Get the bounding box of the span
                    if rect.intersects(span_rect):  # Check if the span intersects with the given rect
                        # Collect unique font name and size
                        unique_fonts.add((span["font"], int(span["size"])))
    
    # Return a list of unique font names and sizes
    return list(unique_fonts)


def page_text_under_rects(directory, page,rect_dict, inv_pr_matrix):
    """ Using the pdf_rect coordinates in a dict, extracts text and fonts
        then updates the dict """
    for rect_nb in rect_dict.keys():
        # Retrieve the pdf_rect
        rect_object = rect_dict[rect_nb]
        rect = fitz.Rect(rect_object['pdf_rect'])
        # Extract words within the rectangle
        try:
            text = page.get_textbox(rect).strip().replace("\n", " ")
            if text != '':
                myfonts = find_fonts_in_rect(rect,page)
            else:
                if USE_MS_VISION:
                    crop_fn = os.path.join(directory, f'crop_{page.number}_{rect_nb}.png')
                    text, crop_ocr_list = get_text_from_image(crop_fn, inv_pr_matrix)
                if text == '':
                    text = f" digest {page.number+1}-{int(rect_nb)+1}"
                myfonts = [["None", 16]]

            rect_object["text"] = text
            rect_object["fonts"] = myfonts

        except:
            pass
    return rect_dict


def file_sha(filename):
    """ Computes a sha256 for a file.
    In simple terms: The unique signature of a file contents.
    We use this to check if the contents of a file has changed"""
    with open(filename, 'rb') as f:
        sha256_hash = hashlib.sha256()
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()


def get_last_modified_date(file_path):
    """ Return the last mod time of a file_path"""
    timestamp = os.path.getmtime(file_path)
    return  datetime.fromtimestamp(timestamp, timezone.utc).strftime('%Y-%m-%d %H:%M:%S UTC')


def list_files(directories_, exclude_folder):
    """ List all files in directories excluding those in the exclude_folder"""
    if isinstance(directories_, list):
        directories = directories_
    else:
        directories = [directories_]
    result = []
    try:
        # Convert exclude_folder to an absolute path for reliable comparison
        if exclude_folder == '':
            exclude_folder_abs = None
        else:
            exclude_folder_abs = os.path.abspath(exclude_folder)

        for directory in directories:
            
            directory_abs = os.path.abspath(directory)

            for root, dirs, files in os.walk(directory_abs):

                # Check if the current root is within the exclude_folder
                if exclude_folder_abs is not None:
                    if exclude_folder_abs in os.path.abspath(root):
                        continue  # Skip this iteration
                
                for name in files:
                    result.append(os.path.join(root, name))
        return result
    except Exception as e:
        print(f"An error occurred: {e}")
        return []


def create_workdir(source_subfolder=SN_VAULT):
    """ Creates a temp timestamped sub-folder """
    current_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    work_folder = os.path.join(source_subfolder, f'work-{current_time}')
    os.makedirs(work_folder, exist_ok=True)
    return work_folder


def calculate_min_distance(rect1, rect2):
    left1, bottom1, right1, top1 = rect1
    left2, bottom2, right2, top2 = rect2

    h_dist = max(0, max(left1, left2) - min(right1, right2))
    v_dist = max(0, max(bottom1, bottom2) - min(top1, top2))
    result = (h_dist**2 + v_dist**2)**0.5

    return result


def assemble_puzzle(pieces, rects, inv_pr_matrix):
    """
    Assemble puzzle pieces into one image by calculating the necessary width and height from the bounding rectangles.

    :param pieces: List of PIL.Image objects for each piece of the puzzle.
    :param rects: List of tuples (x0, y0, x1, y1) defining the bounding box for each piece.
    :return: PIL.Image object representing the assembled image.
    """
    if not pieces or not rects:
        return None  # Return None if no pieces or rects

   
    # Calculate the minimum and maximum x and y values
    min_x = min(rect[0] for rect in rects)
    min_y = min(rect[1] for rect in rects)
    max_x = max(rect[2] for rect in rects)
    max_y = max(rect[3] for rect in rects)

    
    # Calculate the width and height of the output image
    width = max_x - min_x
    height = max_y - min_y
    


    # Create a new blank image with the computed dimensions
    final_image = Image.new('RGBA', (width, height))

    # Iterate over each piece and its corresponding rectangle
    for piece, rect in zip(pieces, rects):
        x0, y0, x1, y1 = rect
        # Resize the piece to match its target size
        resized_piece = piece.resize((x1 - x0, y1 - y0), Image.Resampling.LANCZOS)
        # Paste the resized piece onto the final image, adjusting position relative to the minimum x and y
        final_image.paste(resized_piece, (x0 - min_x, y0 - min_y), resized_piece)

    # Find the relative coordinates in the reporting 
    rel_coordinates = fitz.Rect(0,0,width,height)*inv_pr_matrix

    return final_image, (rel_coordinates.width,rel_coordinates.height)


def create_toc(doc, toc_info, header):
    """ Appends :toc_info as a table of content of of a :doc with a title named :header """
    if toc_info == []:
        return
    
    toc = doc.get_toc()

    len_toc = len(toc)

    # Define the table of contents (TOC)
    # Each entry is a list: [level, title, page, top]

    # Find previous headers
    previous_header_items_list = []
    an_index = 0
    finished = False
    header_level = None
    while (an_index < len_toc) and (not finished):
        an_item = toc[an_index]
        if an_item[1] == header:
            header_level = an_item[0]
            an_index +=1
            if an_index < len_toc:
                toc_level = toc[an_index][0]
                while (toc_level > header_level) and (an_index < len_toc):
                    previous_header_items_list.append(toc[an_index])
                    an_index +=1
                    if an_index < len_toc:
                        toc_level = toc[an_index][0]
                finished = True
            else:
                finished = True
        an_index +=1

    clean_toc = [x for x in toc if x not in previous_header_items_list and x[1] != header]

    if not header_level:
        header_level = 1

    clean_toc.append([header_level, header, toc_info[0][2],{"color": (1, 0, 0)} ])
    toc_info_l = [[header_level+1, x[0],x[2]] for x in toc_info]

    previous_header_items_list.extend(toc_info_l)
    previous_header_items_list.sort(key=lambda x:x[2])

    # previous_header_items_list.sort(key=lambda x:x[2])
    clean_toc.extend(previous_header_items_list)

    # Update the document's TOC
    # This method also accepts a second parameter to replace existing TOC (default) or append
    doc.set_toc(clean_toc, collapse=1)


def create_digest_pdf(pysn_dict, highlights_dict, current_sha, marked_dict, source_fn, digested, doc_e=None, 
                      embed_summary=EMBED_SUMMARY,show_rec_notes=SHOW_REC_NOTES, comment_author=AUTHOR):
    """ Creates a digest summary, using dictionary info in marked_json
     and create the links to the digested pdf """

    toc_info = [] 
    # Get xref of the search layer
    oc_search = xref_layer_by_name(digested, 'pysn_search')
    if oc_search is None:
        oc_search = digested.add_ocg("pysn_search", on=True)

    if doc_e:
        oc_search_e = xref_layer_by_name(doc_e, 'pysn_search')
        if oc_search_e is None:
            oc_search_e = doc_e.add_ocg("pysn_search", on=True)




    # Current directory
    current_dir = os.path.dirname(source_fn)
    output_fn = f'{source_fn[:-4]}-summary.pdf'

    # Create a new pdf, if needed
    if embed_summary:
        summary_doc = digested
        summary_doc_e = doc_e
        offset_summary = digested.page_count
    else:
        summary_doc = fitz.Document()
        basename_fn = os.path.basename(source_fn)[:-4]
        offset_summary = 0

        digested_pdf_path = f'{basename_fn}_.pdf'
        summary_pdf_path = f'{basename_fn}-summary.pdf'



    page_width = PDF_LETTER_WIDTH 
    page_heigth = PDF_LETTER_HEIGHT 
    page_digest_count = 0
    rec_notes_space_used = 100

    summary_page_count = 0
    vertical_page_position = SUMMARY_REPORT_MARGIN +20
    fn_report_caption = source_fn[-25:]

    a_marked_page_nb =  list(marked_dict.keys())[0]

    # Parse the json for marked pages
    for a_marked_page_nb, marked_items in marked_dict.items():
        if a_marked_page_nb in pysn_dict.keys():
            page_settings_dict = pysn_dict[a_marked_page_nb]
        else:
            pysn_dict[a_marked_page_nb] = {}
            page_settings_dict = pysn_dict[a_marked_page_nb] 

        digested_page = digested[int(a_marked_page_nb)]

        digested_page_rect = digested_page.rect     

        page_summary_list = []
        # Parse each marked page for the number of digests on that page
        for page_digest_nb, digest_content in marked_items.items():
            if page_digest_nb != 'highlights' and page_digest_nb != 'annots':

                std_target_rect = digest_content['pdf_rect']
                target_rect = digest_content['pdf_rect_t']
                rotation = (target_rect[0]>target_rect[1]) != (std_target_rect[0]>std_target_rect[1])

                digest_title =  f'...{fn_report_caption} p{int(a_marked_page_nb)+1} - digest #{int(page_digest_nb) + 1}:'
                crop_fn = os.path.join(current_dir,f'crop_{a_marked_page_nb}_{page_digest_nb}.png')  # Get the filename of the crop
                digest_text = digest_content['text']  # Get the text extracted in the original pdf
                
                if rotation:
                    crop_h, crop_w = digest_content['cropped_size'] # Dimensions of the cropped image
                else:
                    crop_w, crop_h = digest_content['cropped_size'] # Dimensions of the cropped image


                # Extract information to display and compute height of element
                # This is an approximation, because some elements, like the height of recognized text, is not known before printing
                computed_height = SUMMARY_REPORT_MARGIN +20 # Initial position
                handwritings_exist = 'hw_image' in digest_content.keys()
                
                handwritings_file_exists = False
                if handwritings_exist:
                    notes_fn = digest_content['hw_image']
                    notes_ocr = digest_content['hw_text']
                    handwritings_file_exists = os.path.exists(notes_fn)
                    if handwritings_file_exists: 
                        if rotation:
                            notes_h_o, notes_w_o = digest_content['hw_size']
                        else:
                            notes_w_o, notes_h_o = digest_content['hw_size']
                        computed_height += notes_h_o

                crop_file_exists = os.path.exists(crop_fn)


                computed_height += VERTICAL_REPORT_SPACING  # Spacing after Title
                
                if crop_file_exists:
                    computed_height += 8 #Position after 'cropped Image' caption
                    computed_height += crop_h #Position after cropped image
                    computed_height += 1 # Image frame
                    computed_height += VERTICAL_REPORT_SPACING # Visual spacing

                print_recognized_text = show_rec_notes and digest_text != '' and digest_text[:7].strip() != 'digest'
                if print_recognized_text:
                    computed_height += 1
                    computed_height += 8
                    computed_height += rec_notes_space_used
                    computed_height += VERTICAL_REPORT_SPACING

                computed_height += 8  # 'Notes' caption
                computed_height += 100  # space for Notes 
                computed_height += VERTICAL_REPORT_SPACING

                page_width = max(PDF_LETTER_WIDTH,digested_page_rect[2] )
                page_heigth = max(PDF_LETTER_HEIGHT,digested_page_rect[3])

                page = summary_doc.new_page(width=page_width, height=page_heigth)
                if doc_e:
                    page_e = summary_doc_e.new_page(width=page_width, height=page_heigth)

                # TODO: Quick fix to display recognized text. Revisit this
                if digest_text != '' and digest_text[:7].strip() != 'digest':
                    standard_rect = fitz.Rect([SUMMARY_REPORT_MARGIN,SUMMARY_REPORT_MARGIN,
                                           PDF_LETTER_WIDTH-SUMMARY_REPORT_MARGIN,PDF_LETTER_HEIGHT/2-SUMMARY_REPORT_MARGIN])

                    page.insert_textbox(standard_rect, digest_text,fontname='cour', fill_opacity=0.01, oc=oc_search) 
                    if doc_e:
                        page_e.insert_textbox(standard_rect, digest_text,fontname='cour', fill_opacity=0.01, oc=oc_search) 



                page_summary_list = []
                page_digest_count = 0
                summary_page_count += 1
                vertical_page_position = SUMMARY_REPORT_MARGIN + 20

                # Store position of the digest's vertical top position
                beg_block_v = vertical_page_position-5
                # Insert digest's title and link of the digest
                rect_title = fitz.Rect(SUMMARY_REPORT_MARGIN,
                                                vertical_page_position-5,
                                                page_width - SUMMARY_REPORT_MARGIN ,
                                                vertical_page_position+25)
                page.insert_textbox(rect_title, digest_title, fontsize=13,color=(0,0,1))
                if doc_e:
                    page_e.insert_textbox(rect_title, digest_title, fontsize=13,color=(0,0,1))
                vertical_page_position += VERTICAL_REPORT_SPACING

                # if embed_summary, create an internal link. If not, create an external link
                
                xd, yd = target_rect[0], target_rect[1]
                xs, ys = rect_title[0], rect_title[1]
                # link_already_exists = rect_exists(current_sha,a_marked_page_nb,target_rect,'pdf_rect_t')
                link_already_exists, intersecting_rect_position = rect_exists(page_settings_dict,target_rect)
                if embed_summary:
                    if not link_already_exists:
                        link_to_digested = {
                            "kind": fitz.LINK_GOTO,
                            "from": rect_title,
                            "page": int(a_marked_page_nb),
                            "to": fitz.Point(xd,yd)}
                        
                        link_to_summary = {
                            "kind": fitz.LINK_GOTO,
                            "from": fitz.Rect(target_rect),
                            "page": summary_page_count-1+offset_summary,
                            "to": fitz.Point(xs,ys)}    
                else:
                    # I think there is a bug in 'insert_link' method of this
                    # PyMupdf version. This is my dirty workaround
                    use_page_nb = int(a_marked_page_nb) 
                    
                    # # # if int(a_marked_page_nb) > 1:
                    # # #     use_page_nb = int(a_marked_page_nb) 
                    # # # else:
                    # # #     use_page_nb = 0
                    use_summary_page_count = summary_page_count -1
                    # # # if summary_page_count > 3:
                    # # #     use_summary_page_count = 3 - 2
                    # # # else:
                    # # #     use_summary_page_count = summary_page_count -1                 
                    link_to_digested = {
                        "kind" : fitz.LINK_GOTOR,
                        "from" : rect_title,
                        "page" : use_page_nb,
                        "file": digested_pdf_path
                        }
                    link_to_summary = {
                        "kind" : fitz.LINK_GOTOR,
                        "from" : fitz.Rect(target_rect),
                        "page" : use_summary_page_count,
                        "file": summary_pdf_path
                        }   
                if not link_already_exists:
  
                    page.insert_link(link_to_digested)  # Insert link from summary to digested    
                    digested[int(a_marked_page_nb)].insert_link(link_to_summary)  # Insert link from digested to summary

                    if doc_e:
                        page_e.insert_link(link_to_digested)  # Insert link from summary to digested    
                        doc_e[int(a_marked_page_nb)].insert_link(link_to_summary)  # Insert link from digested to summary                        

                    if intersecting_rect_position != -1:
                        an_smarker = page_settings_dict['s_marker']
                        an_smarker[intersecting_rect_position][5] = 1
                        an_smarker[intersecting_rect_position].append(link_to_summary['page'])
                        page_settings_dict['s_marker'] = an_smarker

                # Insertion of cropped picture
                if crop_file_exists:
                    # Insert caption image
                    page.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Cropped image:", fontsize=8)
                    vertical_page_position += 8
                    # Insert image
                    page.clean_contents()

                    if doc_e:
                        page_e.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Cropped image:", fontsize=8)
                        page_e.clean_contents()




                    rect_insert = fitz.Rect(SUMMARY_REPORT_MARGIN + 5, 
                                                vertical_page_position,                                        
                                                SUMMARY_REPORT_MARGIN + 5 + crop_w,
                                                vertical_page_position+crop_h)
                    rect_insert_oc = fitz.Rect(SUMMARY_REPORT_MARGIN + 5, 
                                                vertical_page_position,                                        
                                                SUMMARY_REPORT_MARGIN + 5 + max(100,crop_w),
                                                vertical_page_position+max(32,crop_h))
                    
                    page.insert_image(rect_insert,filename=crop_fn, height=80)
                    if doc_e:
                        page_e.insert_image(rect_insert,filename=crop_fn, height=80)

                    # Insert image frame
                    rect_image_frame = fitz.Rect(SUMMARY_REPORT_MARGIN + 4, 
                                            vertical_page_position-1,
                                            page_width-SUMMARY_REPORT_MARGIN-4,
                                            vertical_page_position +crop_h+1)
                    
                    page.draw_rect(rect_image_frame, stroke_opacity=0.05)    
                    if doc_e:
                        page_e.draw_rect(rect_image_frame, stroke_opacity=0.05)
                    
                    vertical_page_position += VERTICAL_REPORT_SPACING +crop_h+1

                    if embed_summary:
                        link_to_digested = {
                            "kind": fitz.LINK_GOTO,
                            "from": rect_image_frame,
                            "page": int(a_marked_page_nb),
                            "to": fitz.Point(xd,yd)}
                    else:
                        # I think there is a bug in 'insert_link' method of this
                        # PyMupdf version. This is my dirty workaround
                        if int(a_marked_page_nb) > 1:
                            use_page_nb = int(a_marked_page_nb) 
                        else:
                            use_page_nb = 0
                        if summary_page_count > 3:
                            use_summary_page_count = 3 - 2
                        else:
                            use_summary_page_count = summary_page_count -1                 
                        link_to_digested = {
                            "kind" : fitz.LINK_GOTOR,
                            "from" : rect_image_frame,
                            "page" : use_page_nb,
                            "file": digested_pdf_path
                            }
                    
                    page.insert_link(link_to_digested)
                    if doc_e:
                        page_e.insert_link(link_to_digested)

                # Inserts text recognized under the "selected areas"
                if print_recognized_text:
                    # Insertion of recognized text
                    page.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Identified text:", fontsize=8)
                    if doc_e:
                        page_e.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Identified text:", fontsize=8)

                    vertical_page_position += 8
                    sizetb = page.insert_textbox(fitz.Rect(SUMMARY_REPORT_MARGIN + 35,
                                                vertical_page_position,
                                                page_width - SUMMARY_REPORT_MARGIN ,
                                                vertical_page_position+rec_notes_space_used), digest_text, fontsize=9,color=(0.254,0.482,0.549))
                    if doc_e:
                        page_e.insert_textbox(fitz.Rect(SUMMARY_REPORT_MARGIN + 35,
                                                vertical_page_position,
                                                page_width - SUMMARY_REPORT_MARGIN ,
                                                vertical_page_position+rec_notes_space_used), digest_text, fontsize=9,color=(0.254,0.482,0.549))


                    rec_notes_space_used -= int(sizetb)
                    vertical_page_position += VERTICAL_REPORT_SPACING + rec_notes_space_used
                else:
                    rec_notes_space_used = 0
                    
                # Insert handwritings section
                page.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5,vertical_page_position), "Notes:", fontsize=8)
                if doc_e:
                    page_e.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5,vertical_page_position), "Notes:", fontsize=8)

                vertical_page_position += 8
                hw_notes_position = vertical_page_position

                # Adds lines in the digest summary notes to guide handwritings
                if DIGEST_LINE_HEIGHT > 0:
                    line_y = vertical_page_position
                    x_start = SUMMARY_REPORT_MARGIN + 5
                    x_end = page_width-SUMMARY_REPORT_MARGIN - 5
                    while line_y < page_heigth-SUMMARY_REPORT_MARGIN-8 :
                        p1 = fitz.Point(x_start, line_y)
                        p2 = fitz.Point(x_end, line_y)
                        page.draw_line(p1,p2,color=(0.254,0.482,0.549),stroke_opacity=0.4)
                        if doc_e:
                            page_e.draw_line(p1,p2,color=(0.254,0.482,0.549),stroke_opacity=0.4)
                        line_y += DIGEST_LINE_HEIGHT



                if handwritings_file_exists:
                    notes_w = int(NOTES_SHRINK_RATIO*notes_w_o)
                    notes_h = int(NOTES_SHRINK_RATIO*notes_h_o)
                    page.clean_contents()
                    
                    rect_insert = fitz.Rect(SUMMARY_REPORT_MARGIN + 5, 
                                                vertical_page_position,
                                                SUMMARY_REPORT_MARGIN + 5 +notes_w,
                                                vertical_page_position+notes_h)
                    sizetb = page.insert_image(rect_insert,filename=notes_fn,keep_proportion=False) 
                    if doc_e:
                        page_e.clean_contents()
                        page_e.insert_image(rect_insert,filename=notes_fn,keep_proportion=False) 


                    page.insert_textbox(rect_insert, notes_ocr,fontname='cour', fill_opacity=0.01, oc=oc_search) 
                    if doc_e:
                        page_e.insert_textbox(rect_insert, notes_ocr,fontname='cour', fill_opacity=0.01, oc=oc_search_e) 
                    
                    vertical_page_position += notes_h 
                        
                vertical_page_position += VERTICAL_REPORT_SPACING + 100
                
                # Insert digest's overall frame
                page.draw_rect(fitz.Rect(SUMMARY_REPORT_MARGIN-5, 
                                        beg_block_v-10,
                                        page_width-SUMMARY_REPORT_MARGIN+5,
                                        page_heigth-SUMMARY_REPORT_MARGIN), 
                                        color=(0.254,0.482,0.549), stroke_opacity=0.35)  
                if doc_e:
                    page_e.draw_rect(fitz.Rect(SUMMARY_REPORT_MARGIN-5, 
                                        beg_block_v-10,
                                        page_width-SUMMARY_REPORT_MARGIN+5,
                                        page_heigth-SUMMARY_REPORT_MARGIN), 
                                        color=(0.254,0.482,0.549), stroke_opacity=0.35)  
                # Saving position of digest handwritings 
                page_summary_list.append((SUMMARY_REPORT_MARGIN-5, hw_notes_position,page_width-SUMMARY_REPORT_MARGIN+5,vertical_page_position ))

                vertical_page_position += VERTICAL_REPORT_SPACING  
            
                # Generate bookmark for the TOC offset_summary
                a_bookmark = (f' •  P{summary_page_count} -  digest {int(a_marked_page_nb)+1}-{int(page_digest_nb)+1}', 'Title 1', summary_page_count + offset_summary)
                toc_info.append(a_bookmark)
                # Increment nb of digests & pages
                page_digest_count += 1

            if 'summary_hw_zone' in page_settings_dict.keys():
                previous_summary_hw_zone = page_settings_dict['summary_hw_zone']
            else:
                previous_summary_hw_zone = []
            
            previous_summary_hw_zone.extend(page_summary_list)  # saving for metadata
            previous_summary_hw_zone = set(tuple(item) for item in previous_summary_hw_zone)
            previous_summary_hw_zone = [list(item) for item in previous_summary_hw_zone]
            page_settings_dict['summary_hw_zone'] = previous_summary_hw_zone
            pysn_dict[a_marked_page_nb] = page_settings_dict
        
    # Now handling the highlights from the SN
    # Get page and corresponding highlight dictionary (ah_page_key is zero start based)
    for ah_page_key, highlights_info in highlights_dict.items():
        # Parse each of the highlights on that page
        for an_item_hl in highlights_info:
            # Get the pdf coordinates of the current highlight
            highlights_rect = an_item_hl['mupdfRectList']
            highlights_rect_list = [[item['x0'], item['y0'], item['x1'], item['y1']] for item in highlights_rect]  # Put the coordinates in a list of rects format
            highlights_rect_list_m =  merge_rects(list(highlights_rect_list))  # Merge adjacent rects together
            highlights_rect_merged = merge_rects(list(highlights_rect_list))[0]

            highlights_rect_list_ = [fitz.Rect((x[0],x[1],x[2],x[3])) for x in highlights_rect_list_m]

            annotation_type = an_item_hl['annotationType']
            creation_date = timestamp_to_pdf_datetime(an_item_hl['time']) 

            if 'annotationContent' in an_item_hl.keys():
                highlights_text = an_item_hl['annotationContent']

                annot_icon = digested[int(ah_page_key)].add_caret_annot(fitz.Point([highlights_rect_merged[0]-20, highlights_rect_merged[1]]))
                annot_icon.set_opacity(0.6)
                if doc_e:
                    annot_icon_e = doc_e[int(ah_page_key)].add_caret_annot(fitz.Point([highlights_rect_merged[0]-20, highlights_rect_merged[1]]))
                    annot_icon_e.set_opacity(0.6)
            else:
                highlights_text = ''

            # The below are highlights stored in the .mark file. We express them out to the pdf only for exporting docs
            if doc_e:
                annot_e = None
                # Type 1 highlights for the SN are "regular" highlights
                if annotation_type == 0 or annotation_type == 2:
                    annot_e = doc_e[int(ah_page_key)].add_highlight_annot(highlights_rect_list_)
                    annot_e.set_opacity(0.3)
                     
                elif annotation_type == 1:
                    # The SN '1' type is of type underline. Here we prefer not to merge the rects
                    highlights_rect_list_ul = [fitz.Rect((x[0],x[1],x[2],x[3])) for x in highlights_rect_list]
                    annot_e = doc_e[int(ah_page_key)].add_underline_annot(highlights_rect_list_ul)
                    # PyMupdf highlights are ugly in supernote, we dim by adjusting down the opacity
                    annot_e.set_opacity(0.3)                    
                hl_dict = {
                    "name": "ForComment",
                    "content": highlights_text,
                    "modDate": creation_date,
                    "title": comment_author}
                if annot_e:
                    annot_e.set_info(info=hl_dict)


    # Close the summary

    # # if not embed_summary: TODO: This is for splitting the export between pure digest
    # #     create_toc(summary_doc, toc_info, 'Digest summary')
    # #     summary_doc.save(output_fn,garbage=4, deflate=True, clean=True)
    # #     summary_doc.close()
    # #     basename_fn = os.path.basename(pdf_path)[:-4]
    # #     source_digest_fn = f'{pdf_path[:-4]}-summary.pdf'
    # #     dest_digest_fn = os.path.join(EXPORT_FOLDER,f'{basename_fn}-summary.pdf')        
    # #     shutil.copy(source_digest_fn, dest_digest_fn)


def find_handwriting_proximity(rect_dict, rects_notes, page, current_dir, imgi, inv_pr_matrix, proximity_threshold=PROXIMITY_THRESHOLD):
    """
    For selection rect in rect_dict, find handwritings rects that are in proximity then
    update the dict with a 'proximity' key/value
    """
    img_rect_used = []
    mark_counter = page.number
    
    imgi_array = np.array(imgi)
    for rect_nb in rect_dict.keys():
        # Retrieve the pix_rect
        rect_object = rect_dict[rect_nb]
        rect = fitz.Rect(rect_object['raw_rect'])
        index_note = 0
        list_images = []
        list_rects = []
        for handwriting in rects_notes:

            if calculate_min_distance(rect, handwriting) <= proximity_threshold:
                if handwriting not in img_rect_used:
                    img_rect_used.append(handwriting)
                    list_rects.append(handwriting)
                    if 'proximity' in rect_object.keys():
                        rect_object['proximity'].append(handwriting)
                    else:
                        rect_object['proximity'] = [handwriting]
                    cropped_img_filename = os.path.join(current_dir,f'p_note_{mark_counter}_{rect_nb}_{index_note}_.png')
                    cropped_img = imgi.crop(handwriting)
                    data = np.array(cropped_img)
                    # Extract RGB components and Alpha channel
                    r, g, b, a = data.T
                    # Set the alpha value of selection pixels to 0 (fully transparent) and remove transparency of notes colors
                    target_color = HIGHLIGHTS_COLOR[DIGEST_COLOR]
                    notes_color = HIGHLIGHTS_COLOR[NOTES_COLOR]
                    selection_pixels = (r == target_color[0]) & (g == target_color[1]) & (b == target_color[2])
                    notes_pixels = (r == notes_color[0]) & (g == notes_color[1]) & (b == notes_color[2])
                    # Clear the pixels (Set them to be fully transparent)
                    data[..., 3][selection_pixels.T] = 0
                    data[..., 3][notes_pixels.T] = 255
                    img_no_transparency = Image.fromarray(data, 'RGBA')
                    img_no_transparency.save(cropped_img_filename)
                    list_images.append(img_no_transparency)
                    #Also remove the notes from imgi
                    # Convert to NumPy array for fast manipulation
                    
                    x1, y1, x2, y2 = handwriting
                    # Make the specified rectangle area transparent
                    # by setting the alpha channel to 0 in that area
                    imgi_array[y1:y2, x1:x2, 3] = 0
            index_note += 1
        # Now merge the handwritten notes, for each digest
        if list_images != []:
            handwritten_image_fn = os.path.join(current_dir,f'hand_notes_{mark_counter}_{rect_nb}.png')
            handwritten_image, hw_size = assemble_puzzle(list_images, list_rects, inv_pr_matrix)
            handwritten_image.save(handwritten_image_fn)
            rect_object['hw_image'] = handwritten_image_fn
            rect_object['hw_size'] = hw_size
            hw_text, crop_ocr_list = get_text_from_image(handwritten_image_fn, inv_pr_matrix)
            rect_object['hw_text'] = hw_text

        rect_dict[rect_nb] = rect_object
        lrect = [int(x) for x in rect]
        x1, y1, x2, y2 = lrect
        imgi_array[y1:y2, x1:x2, 3] = 0 
    # Convert back to an Image object
    imgi = Image.fromarray(imgi_array) 
    return rect_dict, imgi


def merge_rects(rects, proximity=PROXIMITY_RECT_MERGE):
    """ Regroup together rectangles that are intersecting, within
        the distance of a certain proximity"""
    def are_close(rect1, rect2):
        # Expanding rect1's boundaries by 'proximity' pixels
        expanded_rect1 = (
            rect1[0] - proximity, rect1[1] - proximity, rect1[2] +proximity, rect1[3] + proximity
        )
        # Checking if rect2 intersects with expanded rect1
        return not (
            expanded_rect1[2] < rect2[0] or  # rect1 is left of rect2
            expanded_rect1[3] < rect2[1] or  # rect1 is above rect2
            expanded_rect1[0] > rect2[2] or  # rect1 is right of rect2
            expanded_rect1[1] > rect2[3]     # rect1 is below rect2
        )

    merged = True
    while merged:
        merged = False
        new_rects = []
        while rects:
            current = rects.pop()
            merged_any = False
            for idx, other in enumerate(rects):
                if are_close(current, other):
                    # Merging the rects by taking the min x,y and max x,y
                    merged_rect = (
                        min(current[0], other[0]),
                        min(current[1], other[1]),
                        max(current[2], other[2]),
                        max(current[3], other[3])
                    )
                    rects[idx] = merged_rect
                    merged_any = True
                    merged = True
                    break
            if not merged_any:
                new_rects.append(current)
        rects = new_rects

    return list(rects)


def find_rects_with_color(pysn_dict, mark_counter, total_marked_nb, doc, page,  pdf_path, converter, page_bitmap, mark_file_binaries,doc_e=None, page_e=None,
                          selection_rgb=HIGHLIGHTS_COLOR[DIGEST_COLOR], 
                          highlight_rgb=HIGHLIGHTS_COLOR[HIGHLIGHT_COLOR], 
                          erase_rgb=HIGHLIGHTS_COLOR[ERASE_COLOR], transparency_level=TRANSPARENCY_LVL, other_transparency=TRANSPARENCY_OTHER):
    """ This is the core function, called for each marked page.

    It:
        -   Identifies the highlight colors rects and merges them into selection rects
            The important thing to remember is that selection rects come with different coordinates, 
            depending what reference is being used: a picture from the device or the pdf
        -   Crops images of the oginal pdf
        -   Extracts text under the selection rects in the original pdf (if applicable)
        -   Identifies handrwritten text inside or near the selection rects

        mark_counter: The zero based page counter of the marked file 
        total_marked_nb: The total number of marked pages
        page: the page of the pdf filename that is marked        
        pdf_path: the current pdf filename
        converter: the converter used to export pictures from .mark files

        selection_rgb: the specific color used to identify selections

        """
    pages_marked_for_deletion = []
    page_rect = page.rect
    current_dir = os.path.dirname(pdf_path)
    # Define filename of current page image of the marked notes, including background
    image_path = pdf_path+'_p' + str(mark_counter).zfill(total_marked_nb) + '.png'
    # Extract corresponding marked image from .mark file
    img = converter.convert(mark_counter, vo)

    # Get xref of the search layer
    oc_search = xref_layer_by_name(doc, 'pysn_search')
    if oc_search is None:
        oc_search = doc.add_ocg("pysn_search", on=True)

    if doc_e:
        oc_search_e = xref_layer_by_name(doc_e, 'pysn_search')
        if oc_search_e is None:
            oc_search_e = doc_e.add_ocg("pysn_search", on=True)
    
    # Define filename of current page image of the marked notes, NOT including background
    #  (the 'i' at the end is for invisible background)
    image_path_i = pdf_path+'_ip' + str(mark_counter).zfill(total_marked_nb) + '.png'   
    imgi = converter.convert(mark_counter, vi)

    p_nb_k = str(page.number)

    # Check if the user wants to remove a previously created 'digest'
    if p_nb_k in pysn_dict.keys():
        # Get the page settings
        p_nb_k_settings = pysn_dict[p_nb_k]
        # Parse the settings for that page
        for a_marker in p_nb_k_settings['s_marker']:
            # Get location (rect) of the icon signaling a 'digest'
            highlight_icon_rect = a_marker[0]
            highlight_icon_rect_pdf = a_marker[1]
            red_rectangle_rect = a_marker[2]
            a_link_to_summary = a_marker[4]

            # Determine if the icon was 'marked' to be erased by the user
            if check_color_threshold(imgi, highlight_icon_rect, HIGHLIGHTS_COLOR[ERASE_COLOR]):
                # First remove the caret icon
                page_annots = page.annots(types=[fitz.PDF_ANNOT_CARET])
                for annot in page_annots:
                    if annot.rect.intersects(highlight_icon_rect_pdf):
                        page.delete_annot(annot)
                # Then remove the red rectangle
                page_annots = page.annots(types=[fitz.PDF_ANNOT_SQUARE])
                for annot in page_annots:
                    if annot.rect.intersects(red_rectangle_rect):
                        page.delete_annot(annot)
                # Then remove the associated link
                page_links = page.get_links()
                internal_links = [x for x in page_links if x['kind']==1]
                for a_link in internal_links:
                    a_link_rect = a_link['from']
                    # if a_link_rect.intersects(red_rectangle_rect):

                    if intersection_area(a_link_rect, red_rectangle_rect, threshold=0.8):
                        linked_page_summary = a_link['page']
                        page.delete_link(a_link)
                        pages_marked_for_deletion.append((page.number,linked_page_summary))


    if DEBUG_MODE or USE_MS_VISION:
        img.save(image_path)
        imgi.save(image_path_i)

    # Get the pixmap of the current page
    matrix_pix = fitz.Matrix(RESOLUTION_FACTOR,RESOLUTION_FACTOR) 
    pagepix = page.get_pixmap(alpha=True, matrix=matrix_pix) 
    if page_e:
        pagepix_e = page_e.get_pixmap(alpha=True, matrix=matrix_pix)
 
    # pix_rect = annot.rect * page.rotation_matrix * matrx
    # annot.rect = pix_rect*~(page.rotation_matrix * matrx)
    # annot.rect = pix_rect*inv_pr_matrix
    
    matrx = page.rect.torect(pagepix.irect)
    pr_matrix = page.rotation_matrix * matrx
    inv_pr_matrix = ~pr_matrix
    identity_matrix = fitz.Matrix(1, 0, 0, 1, 0, 0)
   
    if USE_MS_VISION:
        page_hw_text, page_hw_map = get_text_from_image(image_path, inv_pr_matrix)
        a_text = page_hw_text.strip()
        if a_text != '':
            try:
                ov_text, ov_rect = concatenate_text_and_get_rect(page_hw_map)

                # TODO: the code was like this: page.insert_textbox(fitz.Rect(ov_rect), a_text,fontname='cour', fill_opacity=0.8, oc=oc_search) 
                # But the position is wrong. Passibly because ratio needs to be taken into account
                # coordinates page_hw_map were computed using inversion matrix
                # For now, just swithcing to standard rect because we need only page reference, not exact position
                standard_rect = fitz.Rect([SUMMARY_REPORT_MARGIN,SUMMARY_REPORT_MARGIN+PDF_LETTER_HEIGHT/2,PDF_LETTER_WIDTH-SUMMARY_REPORT_MARGIN,PDF_LETTER_HEIGHT-SUMMARY_REPORT_MARGIN])

                page.insert_textbox(standard_rect, a_text,fontname='cour', fill_opacity=0.01, oc=oc_search) 

                if doc_e:
                    page_e.insert_textbox(standard_rect, a_text,fontname='cour', fill_opacity=0.01, oc=oc_search_e) 
            except Exception as e:
                print()
                print(page_hw_map)
                ov_text, ov_rect = concatenate_text_and_get_rect(page_hw_map)
                print(f'------ov_text: {ov_text}')
                print(f'------ov_rect: {ov_rect}')
                print(f'**** Error OCR: {e} -- Rect:({standard_rect})----arec_ocr_txt:{a_text}')
        
           
    # Store pixmap content as an Image object
    pdfpixmap = Image.open(BytesIO(pagepix.tobytes(output='png')))
    if page_e:
        pdfpixmap_e = Image.open(BytesIO(pagepix_e.tobytes(output='png')))

    # Store the page marked image with background in a numpy array
    pixels = np.array(img)
    
    # Open the no-background image and convert it to RGBA if it's not already
    if imgi.mode != 'RGBA':
        imgi = imgi.convert('RGBA')
    # Store its pixels in a numpy array
    pixelsi = np.array(imgi)

    # Convert RGB values of to numpy arrays for easier comparison
    selection_rgb = np.array(selection_rgb)
    highlight_rgb = np.array(highlight_rgb)
    erase_rgb = np.array(erase_rgb)

    # Find connected components of the selection color
    selection_mask = (pixels == selection_rgb).all(axis=-1)
    labeled_array, num_features = ndimage.label(selection_mask)
    rects = ndimage.find_objects(labeled_array)

    # Find connected components of the erase color
    # This will get colors to erase ON THE PDF (as opposed to the mark)
    selection_mask_erase = (pixels == erase_rgb).all(axis=-1)
    labeled_array_erase, num_features = ndimage.label(selection_mask_erase)
    rects_erase = ndimage.find_objects(labeled_array_erase)
    # Convert rects to Python native types for JSON compatibility
    native_erasing_rects = [(slice_x.start, slice_y.start, slice_x.stop - 1, slice_y.stop - 1) for slice_y, slice_x in rects_erase]
    # Sort and merge erasing_rects
    pdf_rects_to_erase = merge_rects(native_erasing_rects)


    # Apply transparency based on color conditions using NumPy
    condition = (pixelsi[:, :, :3] == highlight_rgb).all(axis=-1) | (pixelsi[:, :, :3] == erase_rgb).all(axis=-1)
    pixelsi[condition, 3] = (pixelsi[condition, 3] * (1 - other_transparency)).astype(int)

    # Apply additional transparency to pixels within the identified rectangles
    for slice_y, slice_x in rects:
        pixelsi[slice_y, slice_x, 3] = (pixelsi[slice_y, slice_x, 3] * (1 - transparency_level)).astype(int)

    # Erase the slices of white (erasing color)
    for slice_y, slice_x in rects_erase:
        pixelsi[slice_y, slice_x, 3] = 0

    # Save the modified image
    imgi = Image.fromarray(pixelsi)
    # imgi.save(image_path_i)

    # Convert rects to Python native types for JSON compatibility
    native_rects = [(slice_x.start, slice_y.start, slice_x.stop - 1, slice_y.stop - 1) for slice_y, slice_x in rects]

    # Sort and merge rects
    rects = merge_rects(native_rects)
    sorted_rects = sorted(rects, key=lambda rect: (rect[1], rect[0]))

    # Compare sorted_rects with previous ones stored in the metadata
    # This was a concept I had before figuring out how to edit the trail container strokes,
    # and avoid re-creating existing links (since there was no way to remove the selection strokes)
    # Leaving it because it does prevent the user to recreate an existing link
    # The dictionary of pysn 'objects' is kept in the trail metadata of the pdf

    # Store the page_key
    page_key = str(page.number)

        
    if page_key in pysn_dict.keys():
        # There are pysn objects (settings) for that pdf page, retrieve them
        page_settings = pysn_dict[page_key]
        if 'selection' in page_settings.keys():
            # There are 'selection' items (links were previously created), retrieve them
            previous_selection_list = page_settings['selection']

            # Now compare current selection with the previous
            common_items, sorted_rects_specific, previous_selection_specific, union_list = compare_lists(sorted_rects, previous_selection_list)
            # now save the selection
            page_settings['selection'] = union_list
            # Remove common items for further processing (we already have digest summary zone for these)
            sorted_rects = list(sorted_rects_specific)
        else:
            page_settings['selection'] = sorted_rects
    else:
        pysn_dict[page_key] = {"selection": sorted_rects}
        page_settings = pysn_dict[page_key]

    # Keep original rects before snapping
    un_snapped_rects = list(sorted_rects)

    # Storing a mark picture without selection for digestion and deletion
    imgi_to_alter = erase_pixels(imgi,color_to_erase=HIGHLIGHTS_COLOR[DIGEST_COLOR])
    imgi_to_alter = erase_pixels(imgi_to_alter,color_to_erase=HIGHLIGHTS_COLOR[ERASE_COLOR])
    # RLE encode the modified picture
    replacement_bytes, encoded = rle_encode_img(imgi_to_alter)

    if encoded:
        # Replace the ephemeral static image in the .mark file
        # ('ephemeral', because strokes stored in trailcontainer will overwrite it in future rendering of the page)
        replaced, mark_file_binaries = replace_hex_bytes_in_file_block(mark_file_binaries,replacement_bytes,page_bitmap) 
        if not replaced and DEBUG_MODE:
            print()
            print(f'*** could not alter static image for page {page.number}') 

    # The img variable stores marks with a background
    # We paste these bits of backgrounds over the "erased" areas in imgi, 
    # our real ultimate picture with notes and 'clear' background
    # TODO: Rethink the logic here
    copy_rects(img, imgi, pdf_rects_to_erase)

    # **** Now expand each rect to fit the text in the original pdf and store them in a list
    expanded_rects_list = []
    cropped_rects =[]

    x_ratio = pdfpixmap.width/imgi.width
    y_ratio = pdfpixmap.height/imgi.height

    # Initialize the rect counter
    rectno = 0

    # Parse the list of rects and 'snap' them to a box of content in the pdf page

    for a_sorted_rec in sorted_rects:
        
        # Convert the marked image reference to the pdf page coordinates by 'resizing them'
        # * Do not change this: operations at pixel level
        resized_rect_p = (int(a_sorted_rec[0]*x_ratio),int(a_sorted_rec[1]*y_ratio),int(a_sorted_rec[2]*x_ratio),int(a_sorted_rec[3]*y_ratio))

        # Snapping
        an_expanded_rect, pdfpixmap = snap_to_text(pdfpixmap, resized_rect_p)

        # Save the cropped image as a png
        if an_expanded_rect != []:
            if abs(int(an_expanded_rect[0])-int(an_expanded_rect[2]))>1 and abs(int(an_expanded_rect[1])-int(an_expanded_rect[3]))>1:
                im_rect=pdfpixmap.crop(an_expanded_rect) 
                rect_file_name = os.path.join(current_dir,f'crop_{page.number}_{rectno}.png')
                im_rect.save(rect_file_name) 

                # Find the relative coordinates in the reporting 
                rel_coordinates = fitz.Rect(an_expanded_rect)*inv_pr_matrix
                cropped_rect_size = (rel_coordinates.width, rel_coordinates.height)
            else:

                im_rect = None
                cropped_rect_size = ()

        # Conversion from Numpy.int64 to regular int for serialization and storage to json purpose
        a_e_r_l = [int(x) for x in an_expanded_rect ]
        expanded_rects_list.append(a_e_r_l)
        cropped_rects.append(cropped_rect_size)
        rectno += 1

    if cropped_rects and expanded_rects_list:
    
        # Pair each element of `cropped_rects` with its corresponding element in `expanded_rects_list`
        paired_rects = list(zip(expanded_rects_list, cropped_rects))
        
        # Sort the paired list based on the sorting criteria for `expanded_rects_list`
        # This is the sorting of the rects based on thier natural order top left to right down
        # obsolete because need to keep cropped width correspondance: sorted_rects = sorted(expanded_rects_list, key=lambda rect: (rect[1], rect[0]))
        sorted_paired_rects = sorted(paired_rects, key=lambda x: (x[0][1], x[0][0]))
        
        # Unpack the sorted pairs back into two lists
        sorted_expanded_rects_list, sorted_cropped_rects = zip(*sorted_paired_rects)

        # We need the results as lists (since `zip` returns tuples)
        sorted_rects = list(sorted_expanded_rects_list)
        sorted_cropped_rects = list(sorted_cropped_rects)
    else:
        # Handle the empty list case
        sorted_rects, sorted_cropped_rects = [], []

    # At this point, let's load the rects in a dict
    

    # Initialize and populate a dict with the pix and corresponding pdf rects reference
    rect_dict = {}
    for rect_index in range(len(sorted_rects)):
        x = sorted_rects[rect_index]
        un_snapped_x = un_snapped_rects[rect_index] # Unsnapped may still be useful
        theorical_pdf_rect = fitz.Rect(x)*inv_pr_matrix
        pdf_rect_t = [theorical_pdf_rect[0],theorical_pdf_rect[1],theorical_pdf_rect[2],theorical_pdf_rect[3]]
        rect_dict[str(rect_index)] = {
            "raw_rect": un_snapped_x,
            "pix_rect": x, 
            "pdf_rect":[
                x[0]*page_rect[2]/pagepix.w+EXPAND_SHRINK_PXL-3,x[1]*page_rect[3]/pagepix.h+EXPAND_SHRINK_PXL-3,
                x[2]*page_rect[2]/pagepix.w-EXPAND_SHRINK_PXL+3,x[3]*page_rect[3]/pagepix.h-EXPAND_SHRINK_PXL+3],
            "pdf_rect_t": pdf_rect_t,
            "cropped_size": sorted_cropped_rects[rect_index]}

    # Now find texts and fonts  under the rectangles in the original pdf (if not a scanned doc without ocr)
    # The function updates the dict with the identified text and fonts
    rect_dict = page_text_under_rects(current_dir, page,rect_dict, inv_pr_matrix )

    
    # Let's now find all the rects with handwritten notes, based on their color (black by default)

    rects_notes = find_notes_image(img)

    # Now compare these rect notes with each of the selection rects. Keep only those close to the selection rect
    # The function saves notes inside and close to the selection rect as separate png files
    # It then removes those specifics notes from the mark image and returns a "clean" mark picture
    rect_dict, imgi = find_handwriting_proximity(rect_dict,rects_notes, page, current_dir, imgi, inv_pr_matrix, proximity_threshold=PROXIMITY_THRESHOLD)

    # Now we merge the imgi picture with the pdf picture
    # For SUPER_HACK_MODE, we need to remove handwritings 
    # (because the user keeps editing them through the .mark file)

    # Automatically save an image for export
    if page_e:
        imgi_e = imgi

    if SUPER_HACK_MODE:
        imgi_array = np.array(imgi)
        notes_color = HIGHLIGHTS_COLOR[NOTES_COLOR]
        r, g, b, a = imgi_array.T
        notes_pixels = (r == notes_color[0]) & (g == notes_color[1]) & (b == notes_color[2])
        imgi_array[..., 3][notes_pixels.T] = 0  # Setting transparency to zero removes the handwritten notes
        imgi = Image.fromarray(imgi_array, 'RGBA')

    imgi_resized = imgi.resize((pagepix.w, pagepix.h), Image.Resampling.LANCZOS)


    # result = Image.alpha_composite(pdfpixmap, imgi_resized.convert("RGBA"))
    result = imgi_resized.convert("RGBA")

    if page_e:
        imgi_resized_e = imgi_e.resize((pagepix.w, pagepix.h), Image.Resampling.LANCZOS)
        result_e = imgi_resized_e.convert("RGBA")
        destination_rect_e = page_e.rect


    # Store the page rect before possible rotation
    destination_rect = page.rect

    if pagepix.w > pagepix.h:
        result = result.rotate(-90, expand=True)  

        # Change destination rect
        destination_rect = fitz.Rect((0,0,destination_rect.y1, destination_rect.x1))

        if page_e:
            result_e = result_e.rotate(-90, expand=True)  
            destination_rect_e = fitz.Rect((0,0,destination_rect_e.y1, destination_rect_e.x1))


    bytes_io = io.BytesIO()
    

    result.save(bytes_io, format='PNG')

    if page_e:
        bytes_io_e = io.BytesIO()
        result_e.save(bytes_io_e, format='PNG')
        page_e.clean_contents()
        page_e.insert_image(rect=destination_rect_e,stream=bytes_io_e )
        pagepix_e.set_rect(fitz.Rect(0,0,50,50),(255, 255, 0,1))


    page.clean_contents()
    page.insert_image(rect=destination_rect,stream=bytes_io )

    pagepix.set_rect(fitz.Rect(0,0,50,50),(255, 255, 0,1))
    annots_list = []
    # List of carets holders
    s_marker = []

    
    # Adding a red rectangle on pdf to signal digest (and link)
    apmap = page.get_pixmap(alpha=True, matrix=matrix_pix)

    ratio_matrix = fitz.Matrix(a=MAX_HORIZONTAL_PIXELS/apmap.w,d=MAX_VERTICAL_PIXELS/apmap.h)
  
    an_index = 0
    for a_rect_ in expanded_rects_list:
        a_rect_f =  fitz.Rect(a_rect_)
        arectp = a_rect_f*inv_pr_matrix

        ahnotr = page.add_rect_annot(arectp)
        annots_list.append(ahnotr.info['id'])
        ahnotr.set_info(
            {
            "modDate": time_now_to_pdf_datetime(),
            "title":AUTHOR})
        ahnotr.update(border_color=(1,0,0), opacity=0.15, fill_color=(1,0,0)) 

        ahnot_frame = page.add_rect_annot(arectp)
        ahnot_frame.set_border(border={"width":1, "dashes":[], "style": "S"})
        ahnot_frame.update(border_color=(1,0,0), opacity=0.55) 

        r_ =  [a_rect_f[0]-50,a_rect_f[1]-15+a_rect_f.height/2,a_rect_f[0],a_rect_f[1]+a_rect_f.height/2+15]
        r = fitz.Rect(r_)*inv_pr_matrix
        a_point_c = [r[0], r[1]]
        # Add a caret to signal notes/link
        ahnot = page.add_caret_annot(fitz.Point(a_point_c))

        # Storing insertion point of the caret pix coordinates, caret pdf coordinates, rectangle pdf coordinates, index of link on page
        s_marker.append([tuple(ahnot.rect*pr_matrix*ratio_matrix),
                         tuple(ahnot.rect),tuple(ahnotr.rect),an_index, ahnotr.info['id'],0])
        an_index += 1
        ahnot.set_opacity(0.6)
        annots_list.append(ahnot.info['id'])

        if page_e:
            ahnotr_e = page_e.add_rect_annot(arectp)
            ahnotr_e.set_info(
                {
                "modDate": time_now_to_pdf_datetime(),
                "title":AUTHOR})
            ahnotr_e.update(border_color=(1,0,0), opacity=0.15, fill_color=(1,0,0)) 
            ahnot_frame_e = page_e.add_rect_annot(arectp)
            ahnot_frame_e.set_border(border={"width":1, "dashes":[], "style": "S"})
            ahnot_frame_e.update(border_color=(1,0,0), opacity=0.55) 
            # Add a caret to signal notes/link
            ahnot_e = page_e.add_caret_annot(fitz.Point(a_point_c))
            ahnot_e.set_opacity(0.6)

    rect_dict['annots'] = annots_list

    # Updates pdf pysn metadata to reflect s_marker
    if 's_marker' in page_settings.keys():
        previous_s_marker =  page_settings['s_marker']
    else:
        previous_s_marker =  []
    previous_s_marker.extend(s_marker)

    page_settings['s_marker'] = previous_s_marker
    return rect_dict, mark_file_binaries, pages_marked_for_deletion


def find_notes_image(img, selection_rgb=HIGHLIGHTS_COLOR[NOTES_COLOR]):
    
    """
    Same principle as above, this time identifying the rects of notes

    """

    pixels = img.load()
    width, height = img.size
    visited = set()
    rects = []


    def dfs_iterative(start_x, start_y):
        stack = [(start_x, start_y)]
        xmin, ymin, xmax, ymax = float('inf'), float('inf'), -float('inf'), -float('inf')
        while stack:
            x, y = stack.pop()
            if (x, y) in visited or not (0 <= x < width and 0 <= y < height):
                continue
            visited.add((x, y))
            if pixels[x, y] != selection_rgb:
                continue
            xmin, ymin = min(xmin, x), min(ymin, y)
            xmax, ymax = max(xmax, x), max(ymax, y)
            for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                nx, ny = x + dx, y + dy
                if (nx, ny) not in visited:
                    stack.append((nx, ny))
        return xmin, ymin, xmax, ymax

    for x in range(width):
        for y in range(height):
            if (x, y) not in visited and pixels[x, y] == selection_rgb:
                rect = dfs_iterative(x, y)
                if rect[2] - rect[0] >= 0 and rect[3] - rect[1] >= 0:
                    rects.append(rect)
        
    rects = merge_rects(rects, proximity=PROXIMITY_NOTES)
    rect_ser = [list(x) for x in rects]
    return rect_ser


def extract_metadata(source_fn, json_fn):
    """ Extracts .note or .mark metadata, store it in a json_fn """
    metadata = None
    # Retrieving base filename and extension
    basename, extension = os.path.splitext(source_fn)
    if extension.lower() in ['.note', '.mark']:
        try:
            with open(source_fn, 'rb') as f:
                metadata = sn.parse_metadata(f)
            # Saving the metadata
            metadata_json = metadata.to_json(indent=4)
            with open(json_fn, 'w') as file:
                file.write(metadata_json)
            return metadata_json
        except Exception as e:
            print(f'Error in extract_metadata - type: {type(e).__name__}')
            print(f'Error message: {str(e)}')
            exit(1)


def detect_modified_files(notes_list):
    """ Returns the list of files in the watched folder that are
        different from the last properties stored in the registry """
    modified = []
    
    # Read registry value
    registry = read_json(os.path.join(SN_VAULT, SN_REGISTRY_FN))

    files_in_registry_list = [item[0] for item in registry.items() if item[1]['type'] == 'file']
    removed_list = [ x for x in files_in_registry_list if x not in notes_list ]
    # Store the current datetime
    current_datetime = datetime.now(timezone.utc).strftime('%Y-%m-%d %H:%M:%S UTC')
    # For each filename, compute its sha and compare it to the one in the registry


    for a_fn in notes_list:
        a_fn_modification = get_last_modified_date(a_fn)
        # modification
        modification = False
        # Compute the sha
        a_sha = file_sha(a_fn)
        # if sha is in registry, just update last check
        # otherwise enter it
        if a_sha in registry.keys():
            a_file_json = registry[a_sha]
            if a_fn not in a_file_json['filenames']:
                a_file_json['filenames'].append(a_fn)
                modification = True
                print(f">>> Warning, duplicate files: {a_file_json['filenames']}")
            
        else:
            a_file_json = {'type': 'sha','filenames':[a_fn]}
            registry[a_sha] = a_file_json
            modification = True
        

        if a_fn in registry.keys():
            a_fn_n = registry[a_fn]
            a_fn_n['last_check'] = current_datetime
            a_fn_n['modified'] = a_fn_modification
            if a_fn_n['sha'] != a_sha:
                a_fn_n['sha'] = a_sha
                modification = True
        else:
            registry[a_fn] = {'type': 'file', 'sha': a_sha, 'modified': a_fn_modification, 'last_check': current_datetime}
            modification = True
        
        a_file_json['last_check'] = current_datetime
        

        if modification:
            a_file_json['modified'] = a_fn_modification
            
            modified.append(a_fn)
    for missing_file in removed_list:
        sha_for_missing = registry[missing_file]['sha']
        if sha_for_missing in registry.keys():
            sha_entry = registry[sha_for_missing]
            filenames = sha_entry['filenames']
            filenames.remove(missing_file)
            del registry[missing_file]

    return modified, removed_list, registry


def marked_pdf_pages(a_mark_json):
    """ Returns 3 lists from the mark metadata:
        - The annotated page numbers
        - The corresponding positions of the static bitmap file
        - The corresponding positions of the stroke containers 
        - The dictionnary of the page addresses in the mark file
    TODO: Needs to be more robust: this is fragile construct that assumes that all 3 list are sorted
    by the page number """
    
    if a_mark_json is not None:
        if '__footer__' in a_mark_json.keys():
            page_address_dict = a_mark_json['__footer__']
            marked_pdf_pages_list = [int(x[4:]) for x in page_address_dict if x[:4] == 'PAGE' ]
            
        if '__pages__' in a_mark_json.keys():
            pages_list = a_mark_json['__pages__']

            marked_pdf_pages_paths_list = [int(a_page['TOTALPATH']) for a_page in pages_list if 'TOTALPATH' in a_page.keys()]
            marked_pdf_pages_bitmap_list = [int(
                a_page['__layers__'][0]['LAYERBITMAP']) for a_page in pages_list if len(a_page['__layers__']) > 0]
    return marked_pdf_pages_list, marked_pdf_pages_bitmap_list, marked_pdf_pages_paths_list, page_address_dict


def examine_trans_matrix(m):
    """ Check basic properties of an image transformation matrix.
        TODO: See later if any use. Unused for now

    Supported matrices *must* have either
    - m.a == m.d == 0
    or:
    - m.b == m.c == 0
    """
    m = fitz.Matrix(m)  # ensure this is a matrix
    msg = ""
    error = False
    if m.b == m.c == 0:  # means 0/180 rotations or flippings
        if m.a * m.d > 0:  # same sign -> no flippings
            if m.a < 0:  # so both, a and d are negative!
                msg = (4, "rot 180")
            else:
                msg = (0, "nothing")
        else:  # we have a flip
            if m.a < 0:  # horizontal flip
                msg = (1, "left-right")
            else:
                msg = (2, "up-down")
    elif m.a == m.d == 0:  # means 90/270 rotations
        if m.b * m.c < 0:
            if m.b > 0:
                msg = (3, "rot 90")
            else:
                msg = (5, "rot 270")
        else:
            if m.b > 0:
                msg = (6, "up-down, rot 90")
            else:
                msg = (7, "rot 90, up-down")
    else:
        error = True

    if error:
        raise ValueError("unsupported matrix")
    return msg


def snap_to_text(image_pic, original_rect, delta=DELTA_PXL_SEARCH, threshold=None):
    """ Snaps an original rect to the closest text in a pict"""
    # Load the image and convert to a NumPy array
    RGB_base = Image.new('RGB', image_pic.size, (255, 255, 255))  # White background
    RGBA_base = RGB_base.convert('RGBA')


    # If the original image has an alpha channel, use it as a mask to blend the images
    if 'A' in image_pic.getbands():  # Check if the image has an alpha channel
        # The alpha channel is used as a mask to composite the original image over the base
        RGB_image = Image.alpha_composite(RGBA_base, image_pic.convert('RGBA'))
        RGB_image = RGB_image.convert('RGB')
        # print('')
    else:
        # If no alpha channel, just convert directly to RGB
        RGB_image = image_pic.convert('RGB')

    image = np.array(RGB_image)
    
    # Extract the sub-image based on the original rectangle
    x1, y1, x2, y2 = original_rect
    sub_image = image[y1:y2, x1:x2]
    original_height = y2-y1

    # Get the predominant background color in the rectangle
    colors, counts = np.unique(sub_image.reshape(-1, 3), axis=0, return_counts=True)
    background_color = colors[counts.argmax()]

    # If threshold is None, calculate an optimal threshold

    if threshold is None:
        
        # Calculate distances from the background color to all other colors
        distances = np.linalg.norm(colors.astype(float) - background_color.astype(float), axis=1)

        if distances.shape[0] > 1:
            # Filter out the background color distance (which is zero)
            non_background_distances = distances[distances > 0]
            # Calculate mean and standard deviation of these distances
            mean_distance = np.mean(non_background_distances)
            std_deviation = np.std(non_background_distances)
            try:
                maximum = np.max(non_background_distances)
            except:
                maximum = 255   
            # Adjust threshold based on standard deviation
            if std_deviation > mean_distance/2:
                threshold = min(mean_distance + 50*(mean_distance / std_deviation), maximum -50)
            else:
                threshold = mean_distance 
        else:
            threshold = 255 

    # Create a mask for text pixels in the entire image
    color_distances = np.linalg.norm(image.astype(float) - background_color.astype(float), axis=2)
    text_mask = color_distances > threshold

    # Find the bounds of text within the original rectangle
    text_indices = np.argwhere(text_mask[y1:y2, x1:x2])
    if text_indices.size > 0:
        text_min_y, text_min_x = text_indices.min(axis=0)
        text_max_y, text_max_x = text_indices.max(axis=0)
        # Adjust coordinates relative to the full image
        text_min_x += x1
        text_max_x += x1
        text_min_y += y1
        text_max_y += y1
    else:
        # No text within the rectangle, return the original
        return original_rect, image_pic

    # Expand to include all text pixels while respecting image boundaries
    expanded_x1 = max(text_min_x - delta, 0)
    expanded_y1 = max(text_min_y - delta, 0)
    expanded_x2 = min(text_max_x + delta, image.shape[1])
    expanded_y2 = min(text_max_y + delta, image.shape[0])

    # Ensure the expanded rectangle does not intersect with text pixels outside the original bounds
    def is_clear_of_text(x1, y1, x2, y2):
        # Check within the expanded bounds
        region = text_mask[y1:y2, x1:x2]
        return not np.any(region)
    result_rect_before_shrink = (expanded_x1, expanded_y1, expanded_x2, expanded_y2)
    # Adjust if necessary to avoid text pixels while keeping within the expanded bounds
    while not is_clear_of_text(expanded_x1-1, expanded_y1, expanded_x1, expanded_y2) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_x1>1):
        expanded_x1 -= 1 # EXPAND_SHRINK_PXL
    while not is_clear_of_text(expanded_x1, expanded_y1, expanded_x2, expanded_y1+1) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_y1<image.shape[0]):
        expanded_y1 += 1
    while not is_clear_of_text(expanded_x2, expanded_y1, expanded_x2+1, expanded_y2) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_x2<image.shape[1]):
        expanded_x2 += 1 # EXPAND_SHRINK_PXL    
    while not is_clear_of_text(expanded_x1, expanded_y2-1, expanded_x2, expanded_y2) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_y2>1):
        expanded_y2 -= 1
    # draw = ImageDraw.Draw(image_pic)
    expansion_contraction = abs((expanded_y2-expanded_y1)/original_height-1)
    if (expanded_y2 > expanded_y1 + 15) and (expansion_contraction < 0.5):
        result_rect = (expanded_x1, expanded_y1, expanded_x2, expanded_y2)
    else:
        result_rect = result_rect_before_shrink
    # draw.rectangle(result_rect, outline="red", width=1)
   
    return result_rect, image_pic


def inventory_fonts(mark_dict):
    font_inventory = set()
    if mark_dict is not None:
        for page, content in mark_dict.items():
            for key, box_content in content.items():
                if key != 'highlights' and key!= 'annots': 
                    if 'fonts' in box_content.keys():
                        for font in box_content['fonts']:
                            font_inventory.add((font[0], font[1]))

    return font_inventory


def determine_thickness(font_name):
    """
    Simple heuristic to determine font thickness based on common naming conventions.
    This function returns a numerical representation of the thickness, where higher
    numbers indicate a thicker font.
    """
    if "Bold" in font_name:
        return 2  # Assume 'Bold' is thicker than normal and other variants
    elif "Regular" in font_name:
        return 1  # Assume 'Regular' is the standard thickness
    else:
        return 0  # Default case for fonts without a clear thickness indicator


def create_reverse_title_hierarchy(fonts):
    """
    Create a dictionary that allows looking up title type based on font name and size.
    The title levels are determined first by font size, then by font thickness, but the
    returned dictionary maps directly from font specifications to a title level string.
    """
    # Sort the fonts first by size (descending) and then by thickness (descending)
    sorted_fonts = sorted(fonts, key=lambda font: (-font[1], -determine_thickness(font[0])))

    # Create the reverse hierarchy
    reverse_hierarchy = {}
    for level, font in enumerate(sorted_fonts, start=1):
        key = (font[0], font[1])  # Create a tuple of font name and size
        reverse_hierarchy[key] = f"Title {level}"

    return reverse_hierarchy


def extract_text_with_titles(mark_dict, reverse_hierarchy):
    """
    Extracts text along with its corresponding highest hierarchy title from JSON data,
    including the page number as part of the tuple.
    
    :param data: The JSON data as a dictionary.
    :param reverse_hierarchy: A dictionary mapping font name and size to title levels.
    :return: A list of tuples, each containing text, its corresponding highest title, and the page number.
    """
    text_with_titles_pages = []
    for page, content in mark_dict.items():
        for box_nb, box_content in content.items():
            if box_nb != 'highlights' and  box_nb != 'annots':
                text = f" •  P{int(page)+1} - {box_content['text']}"
                # Determine the highest title level for each text block
                highest_title = "Title 1"  # Default value
                highest_level = float('inf')  # Initial comparison value
                if 'fonts' in box_content.keys():
                    for font in box_content['fonts']:
                        font_name_size = (font[0], font[1])
                        title_level = reverse_hierarchy.get(font_name_size)
                        if title_level:
                            # Extract the numeric level from the title string
                            current_level = int(title_level.split(' ')[-1])
                            if current_level < highest_level:
                                highest_level = current_level
                                highest_title = title_level
                    text_with_titles_pages.append((text, highest_title, int(page)+1))

    return text_with_titles_pages


def check_color_threshold(image, rect, target_color, threshold=10, margin=5):
    """
    We assume here pix coordinates
    Checks if the number of pixels of a given color within a specified rectangle area of an image
    is above the threshold percentage.

    :param image: PIL Image object
    :param rect: Tuple of (left, upper, right, lower) specifying the rectangle area
    :param target_color: Tuple of (R, G, B) specifying the target color
    :param threshold: absolute pixels nb threshold as an integer
    :return: True if the number of target color pixels is above the threshold, False otherwise
    """
    # Crop the image to the specified rectangle + margin TODO: Check why caret no exactly in place
    x0, y0, x1, y1 = rect
    a_rect = [x0-margin, y0-margin, x1+margin, y1+margin]

    cropped_image = image.crop(a_rect)
    
    # Convert cropped image to numpy array
    np_image = np.array(cropped_image)

    # If the image has an alpha channel, ignore it for the comparison
    if np_image.shape[-1] == 4:
        np_image = np_image[:, :, :3]    

    # Create a mask for pixels that match the target color
    mask = np.all(np_image == target_color, axis=-1)
    
    # Count the number of matching pixels
    matching_pixels = np.sum(mask)
   
    # Check if the matching percentage is above the threshold
    return matching_pixels > threshold
    

def load_user_settings(path = os.path.join(SN_VAULT, USER_SETTINGS_FN)):
    """ Reads user settings and set the value of the global variable accordingly
        TODO: Warn users not to accept someone's else user_settings without checking
         that it is safe, because malignant code could be hidden in it """
    try:
        user_settings_dict = {}
        if os.path.exists(path):
            user_settings_dict = read_json(path)
            # Parse the keys
            for key, value in user_settings_dict.items():
                if key in allowed_globals and isinstance(value, (str, list, dict, int, float, bool, type(None))):
                    eval(f"{key} = {repr(value)}")            
    except Exception as e:
        print()
        print(f'Cannot load load_user_settings: {e}')
        return {}
    return user_settings_dict


def save_user_settings(user_settings_dict):
    """ Saves user settings in a json"""
    try:
        user_settings_fn = os.path.join(SN_VAULT, USER_SETTINGS_FN)
        save_json(user_settings_fn,user_settings_dict )
    except Exception as e:
        print()
        print(f'*** Error in save_user_settings: {e}')


# The core of the program starts here. As of May 27, this is only to process pdf annotation,
# so really we process .mark and pdf files and ignore the rest.
# TODO: reshape as a __main__ core with some settings available from the command line

def main():

    global SN_IO_UPLOAD_FOLDER
    # Initializing transfer_mode (indicates method of file transfer)
    transfer_mode = None
    changed_icon = False  # Indicator flag for visually showing that PySN is running (Windows, Linux)
    try:
       
        # Now load the user_settings
        user_settings = load_user_settings()

        # Argument parser setup
        epilog = ""
        parser = argparse.ArgumentParser(epilog =epilog,description="PySN command line interpreter (CLI) for updating global variables.\
                                         These settings are stored in SN_vault\\user_settings.json. \
                                         If you are setting multiple values (like for example multiple input folders), separate the values with a space.\
                                         The current setting value for each parameter is showing between [].")
        
        for long_param, details in settings_desc_dict.items():  # Go through the list of global variables exposed to CLI
            
            short_param = details['short']
            description = details['description']
            param_type = details['type']
            
            a_variable_name = param_to_global_var[long_param]  # Retrieve the variable name from the dictionary
            a_variable_value = eval(a_variable_name)  # Retrieve the current variable value (the hard-coded value)

            if long_param in user_settings.keys():
                a_variable_value = user_settings[long_param]  # Overwrite with user's saved values

            if param_type == bool:  # Add a '-no-' prefix option to set boolean values
                parser.add_argument(f'--{long_param}', action='store_true',default=a_variable_value, help=f'{description}. [%(default)s]')
                parser.add_argument(f'--no-{long_param}', action='store_true', default=None,  help='')

            elif long_param == 'input':
                
                parser.add_argument(f'--{long_param}',nargs='+',  type=param_type,default=a_variable_value, metavar=f'{a_variable_name}', 
                                     help=f'{description}.  [{" ".join(a_variable_value)}]')

            else:
                parser.add_argument(f'--{long_param}', type=param_type,default=a_variable_value, metavar=f'{a_variable_name}', help=f'{description}. [%(default)s]')

        parser.add_argument('--reset', action='store_true', help='Reset to hard-coded values: the values that are affected to digest.py global variables (in UPPERCASE when using the help)')  # Add a reset option (back to hard-coded values)
        
        # Parse command line arguments
        args = parser.parse_args()

        # Convert args to a dictionary for easier handling
        args_dict = vars(args)
        
        if args_dict['reset']:
            # The user has decided to revert settings to hard-coded values
            new_user_settings ={} 
        else:
            # Now browse the args_back, handling boolean settings, if needed
            args_dict.pop('reset')
            new_user_settings = dict(args_dict)
            for a_key, a_value in args_dict.items():
                if type(a_value) == bool:
                    a_key_list = a_key.split('_')
                    if 'no' == a_key_list[0]:
                        base_key = '_'.join(a_key_list[1:])
                        new_user_settings[base_key] = False
                        new_user_settings.pop(a_key)
                elif a_value is None:
                    new_user_settings.pop(a_key)
            
            # Now altering global variables
            for a_key, a_value in new_user_settings.items():
                a_global_var_name = param_to_global_var[a_key]
                if a_global_var_name in allowed_globals:
                    globals()[a_global_var_name] = a_value
            
        save_user_settings(new_user_settings)  # Saving the last set of settings in 'user_settings.json'

        try:
            if os.name == 'nt':
                # Windows only: SHORTCUT_PATH stores the path of the PySN shortcut
                #               The script tries to inverse its icon while running (see finally section at the end when it restores it)
                if os.path.exists(SHORTCUT_PATH):

                    try:
                        # subprocess.check_call([sys.executable, "-m", "pip", "install", "winshell"])
                        import winshell
                        with winshell.shortcut(SHORTCUT_PATH) as link:
                            link.icon_location = (os.path.join(PYSN_DIRECTORY, "sni.ico"),0)
                        changed_icon = True
                    except Exception as e:
                        print()
                        print(f'--{os.path.join(PYSN_DIRECTORY, "sni.ico")}--') # sni is the inversed SN icon, used for showing that the
                        print(f'****Error: {e}')

            elif  sys.platform.startswith('linux'):
                
                    # Try to find default shortcut on desktop. If not, take settings path TODO: change that
                    default_desktop_shortcut_dir = os.path.dirname(PYSN_DIRECTORY)
                    desktop_shortcut_path = os.path.join(default_desktop_shortcut_dir,'PySN.desktop')
                    if not os.path.exists(desktop_shortcut_path):
                        desktop_shortcut_path = SHORTCUT_PATH
                    if os.path.exists(desktop_shortcut_path):
                        shell_script_path = os.path.join(PYSN_DIRECTORY, 'run_pysn_script.sh')
                        icon_path  = os.path.join(PYSN_DIRECTORY, 'sni.png')
                        with open(desktop_shortcut_path, 'w') as desktop_entry:
                            desktop_entry.write(
                                f"""[Desktop Entry]
Version=1.0
Type=Application
Name=Run Python Script
Exec={shell_script_path}
Icon={icon_path}
Terminal=true
""")
                        os.chmod(desktop_shortcut_path, 0o755)
                        changed_icon = True
        except Exception as e:
            print()
            print(f'*** Error trying to set shortcut permissions and icon: {e}')




        # PySN will attempt to connect using the following preference: 
        #   1. USB
        #   2. WI-FI
        #   3. Local folders of a mirrored synchronization folder (for me OneDrive)
        #
        # Once a tranfer mode is found, the script copies the files to a local directory
        # It then checks if the copied files have been modified, looking at their
        # respective hash signatures

        # 1. Attempting to find Supernote on USB port
        print('>>> Trying to locate Supernote on USB ...')
        try:
            if platform.system() == 'Windows':                      
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb.exe') 
            else:
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb')             
            usb_supernote, was_adb_running = attached_sn(device_name=DEVICE_SN, path=ADB_PATH,host=ADB_HOST, port=ADB_LOCAL_PORT)  # was_adb_running is a flag to know if the script needs to kill that program when it is done with it
        except Exception as e:
            print()
            print(f'*** Cannot find SN on USB: {e}')
            usb_supernote = None

        if usb_supernote:  # If a Supernote is tethered on a USB port

            # Build the paths to the SN folders
            # The path prefix for internal files is not the same as for files on an sd card
            # If a filepath starts with 'SDcard' the file will be deemed to be external 
            # TODO: warn the user of this important distinction

            # Get the 2 types of prefix
            internal_prefix, external_prefix = get_file_system_in_userspace(usb_supernote)

            remote_watched_folders = []
            # Parse the list of folders
            for a_folder_name in SN_IO_WATCHED_FOLDERS:
                a_folder_name_list = a_folder_name.split('/')
                if a_folder_name_list[0].lower() == 'sdcard':
                    # The folder is deemed to be external if it starts with sdcard
                    # The assumption is that a path always ends with a '/'. However, user can forget the last '/'
                    # Testing for this use case
                    if a_folder_name_list[-1] == '':
                        full_folder_name = os.path.join(external_prefix, '/'.join(a_folder_name_list[1:-1]) + '/')
                    else:
                        full_folder_name = os.path.join(external_prefix, '/'.join(a_folder_name_list[1:]) + '/')
                else:
                    full_folder_name = os.path.join(internal_prefix, '/'.join(a_folder_name_list)[:-1] + '/')
                
                remote_watched_folders.append(full_folder_name)

            # We now store the proper prefix for the upload to come. TODO: Change this later, difficult to read code
            a_folder_name_list = SN_IO_UPLOAD_FOLDER.split('/')
            if a_folder_name_list[0].lower() == 'sdcard':
                upload_base_path = external_prefix
                SN_IO_UPLOAD_FOLDER =  '/'.join(a_folder_name_list[1:-1])
            else:
                upload_base_path = internal_prefix

            
            # Build the paths to the machine local mirror
            local_watched_folder = os.path.join(SN_VAULT,SN_IO_FOLDER)

            if not os.path.exists(local_watched_folder):
                os.makedirs(local_watched_folder)

            for a_folder in remote_watched_folders:
                print()
                print(f' >> copying: {a_folder}')
                
                copy_files_from_device(usb_supernote, a_folder, local_watched_folder)

            export_folder = ''
            transfer_mode = 'usb'

        # 2. No SN found on USB, trying wifi (if enabled)
        elif USE_DIRECT_WIFI_TRANSFER:
            print()
            print('>>> Trying to locate Supernote on WIFI ...')  
            external_prefix = 'REMOVABLE0'

            # Prefixing the upload folder for SDcard, iff applicable
            if SN_IO_UPLOAD_FOLDER.split('/')[0].lower() == 'sdcard':
                # Store where to upload the modified pair of .pdf and .mark files 
                url_upload = f'http://{SN_IO_IP_ADDRESS}:{SN_IO_PORT}/{external_prefix}/{SN_IO_UPLOAD_FOLDER}'
            else:
                url_upload = f'http://{SN_IO_IP_ADDRESS}:{SN_IO_PORT}/{SN_IO_UPLOAD_FOLDER}'

            # Store where to upload the folder of the pdf obtained by 
            # merging the original pdf and the annotation file (.mark file) 
            # Prefixing the export folder for SDcard, if applicable
            if EXPORT_FOLDER.split('/')[0].lower() == 'sdcard':
                url_export = f'http://{SN_IO_IP_ADDRESS}:{SN_IO_PORT}/{external_prefix}/{EXPORT_FOLDER}'       
            else:
                url_export = f'http://{SN_IO_IP_ADDRESS}:{SN_IO_PORT}/{EXPORT_FOLDER}'     
            
            # Build the paths to the machine local mirror
            local_watched_folder = os.path.join(SN_VAULT,SN_IO_FOLDER)
            if not os.path.exists(local_watched_folder):
                os.makedirs(local_watched_folder)

            try:
                # For WIFI, the root url path on the SN webserver is 'Supernote'
                # Subolders: Document, EXPORT, ... etc. AND the SDcard !!!!
                # Stores the list of folders you want to transfer from

                # Now populate the local mirror by downloading from the SN webserver 
                for remote_folder in SN_IO_WATCHED_FOLDERS:
                    # Adding SDcard prefix, if applicable
                    if remote_folder.split('/')[0].lower() == 'sdcard':
                        # The folder is deemed to be external if it starts with SDcard
                        url_download = f'http://{SN_IO_IP_ADDRESS}:{SN_IO_PORT}/{external_prefix}/{remote_folder}'
                    else:
                        url_download = f'http://{SN_IO_IP_ADDRESS}:{SN_IO_PORT}/{remote_folder}'
                    # Downloading folder asynchronously
                    asyncio.run(parse_and_download_files(url_download, local_watched_folder))

                export_folder = ''
                transfer_mode = 'webserver'
            except:
                print()
                print(f'*** Cannot connect to SN webserver: IP: {SN_IO_IP_ADDRESS} - Port: {SN_IO_PORT}')

        # 3. Lastly try is to use folders on your system that are synchronized (or not) with the cloud
        # Note: As of 2024-05-27, the SDcard doesn't synch with the cloud
        if transfer_mode is None:

            # build the list of remote folders
            remote_watched_folders = [os.path.join(REMOTE_BASE_PATH, rel_path) for rel_path in SN_IO_WATCHED_FOLDERS]
            export_folder = os.path.join(REMOTE_BASE_PATH, SN_IO_UPLOAD_FOLDER)

            # Adjust settings for Windows
            if platform.system() == 'Windows':
                remote_watched_folders = linux_to_windows_paths(remote_watched_folders)
                remote_upload_folder = linux_to_windows_paths(export_folder)
            
            transfer_mode = 'folder'
            local_watched_folder = remote_watched_folders

        # Read all files sitting in the watched folder, excluding those in the 'export_folder'
        current_notes_list = list_files(local_watched_folder, export_folder)

        # We only process files that have been modified since last run
        # We call detect_modified_files, which compares existing filenames with
        # sha signatures of previously processed files stored in a double dictionary (registry.json)
        changed_files, files_removed, registry  = detect_modified_files(current_notes_list)

        print(f'>>> {len(changed_files)} changed files')
        print(f'>>> {len(files_removed)} removed files')
        print()

        # Now parsing the modified files
        if len(changed_files) > 0:

            # We store all temp data in a time stamped folder
            temp_work_directory = create_workdir()
            
            dict_marks = {}

            # Parsing changed files
            for changed_file in changed_files:
                
                basename, extension = os.path.splitext(os.path.basename(changed_file))

                # For each file, we create a subfolder of the same name
                changed_file_dir = os.path.join(temp_work_directory, basename)
            
                # For .note files, download the file (No further processing as of 2024-05-27)
                if extension.lower() == '.note':
                    if not os.path.exists(changed_file_dir):
                        os.makedirs(changed_file_dir)            
                    destination_fn = os.path.join(changed_file_dir, basename + '.note')
                    shutil.copy(changed_file, destination_fn)
                    # a_metadata = extract_metadata(changed_file, os.path.join(changed_file_dir, basename + '.json'))

                # For .mark files, download the file and the corresponding pdf
                elif extension.lower() == '.mark':
                    changed_file_dir = changed_file_dir[:-4]
                    if not os.path.exists(changed_file_dir):
                        os.makedirs(changed_file_dir)             
                    destination_fn = os.path.join(changed_file_dir, basename + '.mark')
                    pdf_path = destination_fn[:-5]
                    shutil.copy(changed_file, destination_fn)
                    shutil.copy(changed_file[:-5], pdf_path)
                    
                    # Read the metadata
                    a_metadata = json.loads(extract_metadata(changed_file, destination_fn + '.json'))

                    # Read and store .mark file binaries
                    with open(destination_fn, 'rb') as amark_file:
                        mark_file_binaries = amark_file.read()

                    # Read and store current annotated pdf highlights. 
                    # IMPORTANT:    SN does not store that list as a given marked page attribute, therefore this dictionary
                    #               This is likely because highlights are fundamentally different in the sense that they are not
                    #               strokes per se, but rather pdf attributes.
                    #               As a consequence: when exporting a merged pdf + mark files, remember to process highlights
                    #               that were not part of an marked page in the pdf (we do so in create_digest_pdf)
                    highlights_dict = {}
                    if '__header__' in a_metadata.keys():
                        a_header = a_metadata['__header__']
                        if 'HIGHLIGHTDETAILS' in a_header.keys():
                            highlights_dict = a_header['HIGHLIGHTDETAILS']

                    # Read metadata to extract list of marked pages, their paths and address in the binary file
                    list_pdf_pages, list_bitmap_pages, marked_pdf_pages_paths_list, page_address_dict = marked_pdf_pages(a_metadata)

                    # Loading the mark file
                    marked_notebook = sn.load_notebook(destination_fn)

                    # Instantiating the mark converter
                    converter = ImageConverter(marked_notebook, mark=True)

                    # Determining how many leading zero we will use for naming the temporary files
                    max_digits = len(str(len(list_pdf_pages)))
                    
                    # Initialize the current marked counter (zero based), for the current file
                    mark_counter =  0
                    
                    marked_pages = {}
                
                    # Stores sha signature of the source pdf
                    current_sha = file_sha(pdf_path)

                    # Creates a pdf document object, using the source pdf file. This object will contain our outputpdf
                    doc = fitz.open(pdf_path)
                    if AUTO_EXPORT:
                        # If AUTO_EXPORT is set to True, we also instantiate another pdf document, with the same source, but here doc_e
                        # one will be a full merge of our original .pdf and .mark annotation. 
                        # TODO: This is a poor programming logic introduced later on the fly, instead of rethinking the 
                        #       the refactoring. It clutters the code and badly needs optimization.
                        doc_e = fitz.open(pdf_path) 
                    else:
                        # In most what follows we will often check if doc_e exits to know if we have to output 
                        # such export pdf (i.e. the conditional statement 'if doc_e:' is equivalent to 'if AUTO_EXPORT:')
                        doc_e = None

                    # To make the processed files non-device specific, PySN stores information about
                    # the rect location of the created shortcuts and the corresponding linked pages
                    # See 'pysn' jey and value in the extended metadata dictionary.
                    #
                    # TODO: As apparently even Adobe Pro doesn't see the info, this could potentially
                    #       become a privacy issue if you mod the script and store something else.
                    #       Users and developers should be made aware of this.
                    
                    # Retrieve the extended metadata of the pdf
                    arefxx, e_metadata = extended_metadata(doc)
                    if 'pysn' in e_metadata.keys():
                        pysn_dict = json.loads(e_metadata['pysn'])
                    else:
                        pysn_dict = {}
                    
                    # Initialize the list of pages to delete (when the user has 'marked for deletion' the caret on the left side
                    # of a digested link)
                    pages_to_delete = []
                    
                    # OKAY: This is the main loop where we are parsing all the all the marked pages
                    for a_page in list_pdf_pages:

                        if a_page>len(doc):  # TODO: Not sure if we should keep this. Was initially here when we did not have the logic in place to 'mute' a marked page in the binaries
                            continue

                        page_bitmap = list_bitmap_pages[mark_counter]

                        print(f"\r{pdf_path} ------------- p {a_page}") 
                        
                        # Retrieve highlights of the current marked page
                        hl_key = str(a_page-1)
                        if hl_key in highlights_dict.keys():
                            hl_page_info = highlights_dict[hl_key]
                        else:
                            hl_page_info =[]
                        
                        # Get picture of a_page of the original pdf
                        page = doc[a_page-1]
                        if AUTO_EXPORT:
                            page_e = doc_e[a_page-1]
                        else:
                            page_e = None

                        

                        # This is the core of the logic and it is ugly programming.
                        # Look into the details of 'find_rects_with_color', but basically what we do is interpreting the 'selecting' and 'erasing' strokes of the user
                        pdic_rect, mark_file_binaries, pages_marked_for_deletion = find_rects_with_color(
                            pysn_dict, mark_counter, max_digits, doc, page, pdf_path, converter, page_bitmap, mark_file_binaries, doc_e=doc_e, page_e=page_e,
                            selection_rgb=HIGHLIGHTS_COLOR[DIGEST_COLOR])
                        
                        pages_to_delete.extend(pages_marked_for_deletion)
                        pdic_rect['highlights'] = hl_page_info

                        marked_pages[str(a_page-1)] = pdic_rect

                        mark_counter += 1  

                    # The idea behind the next 2 lines was to see if we could create a hierachy of Table Of Contents (TOC), based on the styles
                    # TODO: Investigate later if this is worth it  
                    inventory_fonts_set = inventory_fonts(marked_pages)
                    reverse_hierarchy = create_reverse_title_hierarchy(inventory_fonts_set)

                    toc_info = extract_text_with_titles(marked_pages, reverse_hierarchy)

                    # Create the 'Digested' table of contents ... the bookmarks of the digest location of the pdf (not the summary digest where the notes are taken)
                    create_toc(doc, toc_info,'Digested')
                    if doc_e:
                        create_toc(doc_e, toc_info,'Digested')

                    create_digest_pdf(pysn_dict, highlights_dict, current_sha, marked_pages, pdf_path, doc, doc_e=doc_e, embed_summary=True, comment_author=AUTHOR)
                    
                    basename_fn = os.path.basename(pdf_path)[:-4]
                    source_pdf_merged_fn = pdf_path[:-4]+'_.pdf'
                    sorted_annots_toc(doc)

                    if doc_e:
                        source_pdf_merged_fn_e = pdf_path[:-4]+'_e.pdf'
                        sorted_annots_toc(doc_e)

                    # Deleting pages that were marked for deletion
                    page_set_to_delete = tuple(set(pages_to_delete))
                    
                    if len(page_set_to_delete) > 0:
                        # Deleting pdf pages
                        pages_delete_list = [x[1] for x in page_set_to_delete]
                        doc.delete_pages(pages_delete_list)
                        if doc_e:
                            doc_e.delete_pages(pages_delete_list)

                        # Now let's look into adjusting binaries and pysn dictionary
                        # parsing the list of pages to delete (actually a list of tuples (source and dest))
                        for a_page_to_delete in page_set_to_delete:
                            page_source = a_page_to_delete[0]
                            page_dest = a_page_to_delete[1]
                            a_page_key = f'PAGE{page_dest+1}'
                            # 'Deleting' (muting) corresponding deleted pages in the .mark binaries
                            if a_page_key in page_address_dict.keys():
                                page_address = int(page_address_dict[a_page_key])
                                mark_file_binaries = mute_page(mark_file_binaries, page_address)
                            
                            # Updating the pysn_dict for the current page
                            page_settings_key = str(page_source)
                            if page_settings_key in pysn_dict.keys():
                                page_settings = pysn_dict[page_settings_key]
                                element_to_pop = None
                                if 's_marker' in page_settings:
                                    page_settings_marks = page_settings['s_marker']
                                    page_settings_marks_len = len(page_settings_marks)
                                    index_list = []
                                    for i in range(page_settings_marks_len):
                                        if len(page_settings_marks[i])==7:
                                            if page_settings_marks[i][6] == page_dest:
                                                index_list.append(i)
                                    
                                if index_list:
                                    element_to_pop = index_list[0]
                                    if page_settings['selection']:
                                        page_settings['selection'].pop(element_to_pop)
                                    if page_settings['s_marker']:
                                        page_settings['s_marker'].pop(element_to_pop)
                                    if page_settings['summary_hw_zone']:
                                        page_settings['summary_hw_zone'].pop(element_to_pop)

                                    pysn_dict[page_settings_key] = page_settings

                    # Save pysn dict
                    save_to_extended_metadata(doc, arefxx, 'pysn', pysn_dict)
                    # Saving pdf document
                    doc.save(source_pdf_merged_fn,garbage=4, deflate=True, clean=True)
                    doc.close()  

                    if doc_e:
                        save_to_extended_metadata(doc_e, arefxx, 'pysn', pysn_dict)
                        doc_e.save(source_pdf_merged_fn_e,garbage=4, deflate=True, clean=True)
                        doc_e.close()  


                    # Removing selection and erasing color TODO: defaults assume also 202 (marker light gray) and 254 (marker white)
                    completed, mark_file_binaries = mute_stroke_color(mark_file_binaries,marked_pdf_pages_paths_list)

                    # Saving .mark binaries
                    with open(destination_fn, 'wb') as file:
                        file.write(mark_file_binaries)

                    # Saving sha signature of newly created pdf files in the registry
                    pdf_sha = file_sha(source_pdf_merged_fn)
                    file_mod = get_last_modified_date(source_pdf_merged_fn)
                    registry[pdf_sha] = {"type": "sha",
                                        "filenames": [source_pdf_merged_fn],
                                        "last_check": file_mod,
                                        "modified": file_mod}
                    registry[source_pdf_merged_fn] = {"type": "file",
                                        "sha": pdf_sha,
                                        "last_check": file_mod,
                                        "modified": file_mod}
                    # Cache the pdf
                    shutil.copy(source_pdf_merged_fn, os.path.join(PDF_CACHED,f'{basename_fn}_.pdf')) 

                    # Saving sha signature of new .mark binary
                    mark_sha = file_sha(destination_fn)
                    file_mod = get_last_modified_date(destination_fn)
                    registry[mark_sha] = {"type": "sha",
                                        "filenames": [destination_fn],
                                        "last_check": file_mod,
                                        "modified": file_mod}
                    registry[destination_fn] = {"type": "file",
                                        "sha": mark_sha,
                                        "last_check": file_mod,
                                        "modified": file_mod}

                    # Check if need to save full pdf export file in the registry
                    if AUTO_EXPORT:
                        # Saving sha signature of newly created pdf files in the registry
                        pdf_sha = file_sha(source_pdf_merged_fn_e)
                        file_mod = get_last_modified_date(source_pdf_merged_fn_e)
                        registry[pdf_sha] = {"type": "sha",
                                            "filenames": [source_pdf_merged_fn_e],
                                            "last_check": file_mod,
                                            "modified": file_mod}
                        registry[source_pdf_merged_fn_e] = {"type": "file",
                                            "sha": pdf_sha,
                                            "last_check": file_mod,
                                            "modified": file_mod}
                        # Cache the pdf
                        shutil.copy(source_pdf_merged_fn_e, os.path.join(PDF_CACHED,f'{basename_fn}_e.pdf'))             

                    # Now exporting to the SN/Folders
                    if transfer_mode == 'usb':  #  For transfers via USB
                        try:
                            destination_path = os.path.join(os.path.join(upload_base_path, SN_IO_UPLOAD_FOLDER),f'{basename_fn}_.pdf').replace('\\','/')
                            # Pushing the pdf file to the device
                            usb_supernote.push(source_pdf_merged_fn, destination_path)

                            if AUTO_EXPORT:
                                a_folder_name_list = EXPORT_FOLDER.split('/')
                                if a_folder_name_list[0].lower() == 'sdcard':
                                    upload_base_path_e = os.path.join(external_prefix, '/'.join(a_folder_name_list[1:]))     
                                else:
                                    upload_base_path_e = os.path.join(internal_prefix, '/'.join(a_folder_name_list)) 


                                destination_path_e = os.path.join(upload_base_path_e,f'{basename_fn}_e.pdf').replace('\\','/')
                                # Pushing the full pdf export file to the device

                                usb_supernote.push(source_pdf_merged_fn_e, destination_path_e)

                            # In superhack mode, push the .mark file
                            if SUPER_HACK_MODE: 
                                print()
                                mark_source_fn = f'{source_pdf_merged_fn[:-5]}.pdf.mark'
                                mark_dest_fn = f'{destination_path}.mark'
                                usb_supernote.push(mark_source_fn, mark_dest_fn)
                    
                        except:
                            print()
                            print(f"  *** Could not push {f'{basename_fn}_.pdf'} to the Supernote on USB ***")     

                    elif transfer_mode == 'webserver':  #  For transfers via WIFI
                        try:
                            asyncio.run(async_upload(source_pdf_merged_fn, url_upload, f'{basename_fn}_.pdf'))
                            asyncio.run(async_upload(f'{source_pdf_merged_fn[:-5]}.pdf.mark', url_upload, f'{basename_fn}_.pdf.mark'))
                            if AUTO_EXPORT:
                                asyncio.run(async_upload(source_pdf_merged_fn_e, url_export, f'{basename_fn}_e.pdf'))
                        except:
                            print()
                            print(f"  *** Could not upload {f'{basename_fn}<_e>.pdf'}. Is the SN access - Browse mode on? ***")
                            exit(1)

                    elif transfer_mode == 'folder':
                        try:
                            dest_pdf_merged_fn = os.path.join(remote_upload_folder,f'{basename_fn}_.pdf')
                            shutil.copy(source_pdf_merged_fn, dest_pdf_merged_fn) # TODO: if no debug mode, just save to the export folder
                        except:
                            print()
                            print(f'  *** Could not save file: {dest_pdf_merged_fn}. If you have it open, please close and try again. ***')

                    print()
            
            # Remove all temporary files
            if not DEBUG_MODE:
                shutil.rmtree(temp_work_directory)
        
        # Save the registry. We save it only at the end so that any failure to complete will allow reprocessing the files
        save_json(os.path.join(SN_VAULT, SN_REGISTRY_FN), registry)
    finally:
        # Kill ADB if it was instantiated by PySN
        if transfer_mode == 'usb':
            if platform.system() == 'Windows':                      
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb.exe')      
            else:
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb') 
            stop_adb_if(original_state=was_adb_running,path=ADB_PATH)
        
        # Restoring the normal icon (for Windows)
        if changed_icon:
            try:
                if os.name == 'nt':
                    with winshell.shortcut(SHORTCUT_PATH) as link:
                        link.icon_location = (os.path.join(PYSN_DIRECTORY, "sn.ico"),0)

                elif  sys.platform.startswith('linux'):
                    # Try to find default shortcut on desktop. If not, take settings path TODO: change that
                    default_desktop_shortcut_dir = os.path.dirname(PYSN_DIRECTORY)
                    desktop_shortcut_path = os.path.join(default_desktop_shortcut_dir,'PySN.desktop')
                    if not os.path.exists(desktop_shortcut_path):
                        desktop_shortcut_path = SHORTCUT_PATH

                    if os.path.exists(desktop_shortcut_path):
                        shell_script_path = os.path.join(PYSN_DIRECTORY, 'run_pysn_script.sh')
                        icon_path  = os.path.join(PYSN_DIRECTORY, 'sn.png')
                        with open(desktop_shortcut_path, 'w') as desktop_entry:
                            desktop_entry.write(f"""[Desktop Entry]
Version=1.0
Type=Application
Name=Run Python Script
Exec={shell_script_path}
Icon={icon_path}
Terminal=true
""")
                        os.chmod(desktop_shortcut_path, 0o755)
            except Exception as e:
                print()
                print(f'*** Error trying to restore normal shortcut icon and permissions: {e}')
        
    
    
if __name__ == "__main__":
    main()


